package askwabco;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.creativityapps.gmailbackgroundlibrary.BackgroundMail;
import com.jaiselrahman.filepicker.model.MediaFile;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.io.File;
import java.util.ArrayList;

import alertbox.Alertbox;
import directory.WabcoUpdate;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import home.MainActivity;
import network.NetworkConnection;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import search.SearchActivity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;

public class AskWabcoActivity extends Activity {

    private EditText NameEdit, PhoneEdit, EmailEdit, SubEdit, DiscEdit, UserEdit, FileEdit;
    private Button SendBtn, CancelBtn;
    private Button BrowseBtn;
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private static final int PICKIMAGE_RESULT_CODE = 1;
    private static final int PICKFILE_RESULT_CODE = 2;
    private int columnIndex;
    private Uri URI = null;
    private String attachmentFilePath;
    private int STORAGE_PERMISSION_CODE = 23;
    private static final int REQUEST_WRITE_STORAGE = 112;
    private ImageView backImageView;
    private ProgressDialog progressDialog;
    private Alertbox alertbox = new Alertbox(AskWabcoActivity.this);
    private File attachFile;
    private String subject;
    private CheckBox AddattachCh;
    private LinearLayout Attachlayout;
    private AlertDialog alertDialog;
    private boolean isSend;
    private ArrayList<MediaFile> mediaFiles = new ArrayList<>();
    private final static int FILE_REQUEST_CODE = 1;
    private ArrayList<String> mAttachments = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_wabco);


        NameEdit = (EditText) findViewById(R.id.nameedit);
        PhoneEdit = (EditText) findViewById(R.id.phoneedit);
        EmailEdit = (EditText) findViewById(R.id.emailedit);
        SubEdit = (EditText) findViewById(R.id.subjectedit);
        DiscEdit = (EditText) findViewById(R.id.discedit);
        UserEdit = (EditText) findViewById(R.id.useredit);
        FileEdit = (EditText) findViewById(R.id.filenameedit);

        Attachlayout = (LinearLayout) findViewById(R.id.attachlayout);


        UserEdit.setEnabled(false);

        SendBtn = (Button) findViewById(R.id.okay);
        CancelBtn = (Button) findViewById(R.id.cancel);
        BrowseBtn = (Button) findViewById(R.id.browse);

        AddattachCh = (CheckBox) findViewById(R.id.attach);

        AddattachCh.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    alertDialog = new AlertDialog.Builder(AskWabcoActivity.this).create();
                    alertDialog.setTitle("WABCO");
                    alertDialog.setMessage("You should have a default email configured in your mobile to send email with attachment.");
                    alertDialog.setButton("Okay", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            Attachlayout.setVisibility(View.VISIBLE);
                        }
                    });
                    alertDialog.show();

                } else {
                    Attachlayout.setVisibility(View.GONE);
                }
            }
        });

        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();


        NameEdit.setText(myshare.getString("name", ""));
        PhoneEdit.setText(myshare.getString("phone", ""));
        EmailEdit.setText(myshare.getString("email", ""));
        UserEdit.setText(myshare.getString("usertype", ""));


        BrowseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<String> optionList = new ArrayList<>();
                optionList.add("Images");
                optionList.add("File");

                new MaterialDialog.Builder(AskWabcoActivity.this)
                        .title(R.string.app_name)
                        .items(optionList)
                        .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

                            if(text.equals("Images"))
                            {
                                FilePickerBuilder.getInstance().setMaxCount(5)
                                        .setSelectedFiles(mAttachments)
                                        .setActivityTheme(R.style.LibAppTheme)
                                        .pickPhoto(AskWabcoActivity.this);
                            }
                            else
                            {
                                FilePickerBuilder.getInstance().setMaxCount(10)
                                        .setSelectedFiles(mAttachments)
                                        .setActivityTheme(R.style.LibAppTheme)
                                        .pickFile(AskWabcoActivity.this);
                            }

                                return true;
                            }
                        })
                        .positiveText(R.string.choose)
                        .show();
            }
        });


        SendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (NameEdit.getText().length() == 0) {
                    NameEdit.setError("Enter Your Name !");
                    Toast.makeText(AskWabcoActivity.this, "Enter Your Name !", Toast.LENGTH_LONG).show();
                } else if (!(PhoneEdit.getText().length() == 10)) {
                    PhoneEdit.setError("Enter Your Mobile Number !");
                    Toast.makeText(AskWabcoActivity.this, "Invalid Mobile Number !", Toast.LENGTH_LONG).show();
                } else if (!isValidEmail(EmailEdit.getText())) {
                    EmailEdit.setError("Enter Your Email Id !");
                    Toast.makeText(AskWabcoActivity.this, "Invalid Email Id !", Toast.LENGTH_LONG).show();
                } else if (SubEdit.getText().length() == 0) {
                    SubEdit.setError("Enter Subject !");
                    Toast.makeText(AskWabcoActivity.this, "Enter Subject !", Toast.LENGTH_LONG).show();
                } else if (UserEdit.getText().length() == 0) {
                    UserEdit.setError("Enter Subject !");
                    Toast.makeText(AskWabcoActivity.this, "Enter Subject !", Toast.LENGTH_LONG).show();
                } else if (DiscEdit.getText().length() == 0) {
                    DiscEdit.setError("Enter Assistance Required  !");
                    Toast.makeText(AskWabcoActivity.this, "Enter Assistance Required  !", Toast.LENGTH_LONG).show();
                } else {
                    checkinternet();
                }

            }
        });


        CancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        backImageView = (ImageView) findViewById(R.id.back);

        backImageView.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                onBackPressed();
            }
        });

        final ImageView menu = (ImageView) findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(AskWabcoActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(AskWabcoActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(AskWabcoActivity.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(AskWabcoActivity.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(AskWabcoActivity.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(AskWabcoActivity.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(AskWabcoActivity.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(AskWabcoActivity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(AskWabcoActivity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(AskWabcoActivity.this, PriceListActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(AskWabcoActivity.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });


    }

    private void checkinternet() {
        NetworkConnection connection = new NetworkConnection(AskWabcoActivity.this);

        if (connection.CheckInternet()) {
            if (AddattachCh.isChecked()) {
                SendMail();
            } else {
                // new SendPassword().execute();
                SendMail();
            }


        } else {
            // Toast.makeText(LoginActivity.this,"Not unable to connect please check your Internet connection ! ", Toast.LENGTH_SHORT).show();
            alertbox.showAlertbox(getResources().getString(R.string.nointernetmsg));


        }


    }




    protected void sendEmail() {
        Log.i("Send email", "");


//customer.care@wabco-auto.com

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"customer.care@wabco-auto.com"});
        // emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, SubEdit.getText().toString());
        String msg = " Name         : " + NameEdit.getText().toString() + " \n Mobile No    : " + PhoneEdit.getText().toString() + "\n User Type    : " + UserEdit.getText().toString()
                + "\n Assistance Required  : " + DiscEdit.getText().toString();
        Log.v("Body", msg);

        emailIntent.putExtra(Intent.EXTRA_TEXT, msg);

        if (mAttachments.size()!=0)
            emailIntent.putExtra(Intent.EXTRA_STREAM,mAttachments.get(0));


        try {
            startActivity(Intent.createChooser(emailIntent, "Sending email..."));
            finish();
            Log.i("Finish sending email...", "");
        } catch (android.content.ActivityNotFoundException ex) {
            //Toast.makeText(AskWabcoActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
            alertbox.showAlertbox("You don't have a default email configured in your mobile. Cannot attach file !");
        }
    }


    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode)
        {
            case FilePickerConst.REQUEST_CODE_PHOTO:
                if(resultCode== Activity.RESULT_OK && data!=null)
                {
                    mAttachments = new ArrayList<>();
                    mAttachments.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
                }
                break;
            case FilePickerConst.REQUEST_CODE_DOC:
                if(resultCode== Activity.RESULT_OK && data!=null)
                {
                    mAttachments = new ArrayList<>();
                    mAttachments.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));
                }
                break;
        }
        FileEdit.setText(mAttachments.toString());

    }


    public void SendMail() {
        progressDialog = new ProgressDialog(AskWabcoActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Sending email...");
        progressDialog.show();

        String msg = " Name         : " + NameEdit.getText().toString() + " \n Mobile No    : " + PhoneEdit.getText().toString() + "\n User Type    : " + UserEdit.getText().toString()
                + "\n Assistance Required  : " + DiscEdit.getText().toString();
        Log.v("Body", msg);
        String subject = SubEdit.getText().toString();
       // String mail = "customer.care@wabco-auto.com,sivakumar.s@wabco-auto.com,rkumaravel@brainmagic.info";
        String mail = "velu@brainmagic.info,mohan@brainmagic.info";

        try {
            BackgroundMail backgroundMail =   BackgroundMail.newBuilder(AskWabcoActivity.this)
                    .withUsername("wabcoindiachennai@gmail.com")
                    .withPassword("Wabco_catalogue")
                    .withMailto(mail)
                    .withType(BackgroundMail.TYPE_PLAIN)
                    .withSubject(subject)
                    .withBody(msg)
                    .withOnSuccessCallback(new BackgroundMail.OnSuccessCallback() {
                        @Override
                        public void onSuccess() {
                            //do some magic
                            progressDialog.dismiss();
                            alertbox.showAlertbox("Your mail has been sent successfully !");
                        }
                    })
                    .withOnFailCallback(new BackgroundMail.OnFailCallback() {
                        @Override
                        public void onFail() {
                            //do some magic
                            progressDialog.dismiss();
                            //alertbox.showAlertbox("Your mail was not sent!");
                            sendEmail();
                        }
                    })
                    .send();

            if(mAttachments.size()!=0)
            backgroundMail.addAttachments(mAttachments);

        } catch (Exception e) {
            progressDialog.dismiss();
            alertbox.showAlertbox(getResources().getString(R.string.nointernetmsg));
        }


    }


    class SendPassword extends AsyncTask<String, Void, String> {
        private int i = 0;
        private String emailid;
        private String msg;
        private boolean isSend;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(AskWabcoActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Sending email...");
            progressDialog.show();

            msg = " Name         : " + NameEdit.getText().toString() + " \n Mobile No    : " + PhoneEdit.getText().toString() + "\n User Type    : " + UserEdit.getText().toString()
                    + "\n Assistance Required  : " + DiscEdit.getText().toString();
            Log.v("Body", msg);
            subject = SubEdit.getText().toString();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {

               /* StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                // String mail = emailid;
                String mail = "customer.care@wabco-auto.com,sivakumar.s@wabco-auto.com,rkumaravel@brainmagic.info";
                Mail localMail1 = new Mail("wabcoindiachennai@gmail.com", "Wabco_catalogue");
                String[] arrayOfString = mail.split(",");

                localMail1.setTo(arrayOfString);
                localMail1.setFrom("wabcoindiachennai@gmail.com");
                localMail1.setSubject(subject);

                if (attachFile != null)
                    localMail1.addAttachment(attachFile.getAbsolutePath());

                Log.v("body", msg);

                localMail1.setBody(msg);
                if (localMail1.send()) {
                    return "send";
                } else {
                    return "notsend";
                }
*/


                BackgroundMail.newBuilder(AskWabcoActivity.this)
                        .withUsername("wabcoindiachennai@gmail.com")
                        .withPassword("Wabco_catalogue")
                        .withMailto("velu@brainmagic.info,mohan@brainmagic.info")
                        .withType(BackgroundMail.TYPE_PLAIN)
                        .withSubject("this is the subject")
                        .withBody("this is the body")

                        .withOnSuccessCallback(new BackgroundMail.OnSuccessCallback() {
                            @Override
                            public void onSuccess() {
                                //do some magic
                                isSend = true;
                            }
                        })
                        .withOnFailCallback(new BackgroundMail.OnFailCallback() {
                            @Override
                            public void onFail() {
                                //do some magic
                                isSend = false;
                            }
                        })
                        .send();

                if (isSend) {
                    return "send";
                } else {
                    return "notsend";
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equals("send")) {
                alertbox.showAlertbox("Your mail has been sent successfully !");
            } else if (s.equals("notsend")) {
                alertbox.showAlertbox("Your mail was not sent successfully !");
            } else {
                alertbox.showAlertbox(getResources().getString(R.string.nointernetmsg));
            }

        }
    }


}
