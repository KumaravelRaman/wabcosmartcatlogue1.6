package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.List;



/**
 * Created by SYSTEM09 on 11/27/2017.
 */

public class EquivalentAdapter extends ArrayAdapter<String> {
    private  List<String> originalList,equivalentList;
    private Context context;

    public EquivalentAdapter(Context context, List<String> originalList, List<String> equivalentList) {
        super(context, R.layout.layout_equivalet_adapter, originalList);
        this.context = context;
        this.originalList = originalList;
        this.equivalentList = equivalentList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        EquivalentHolder equivalentHolder;
        if (convertView == null) {
            convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater"))
                    .inflate(R.layout.layout_equivalet_adapter, null);
            equivalentHolder = new EquivalentHolder();
            equivalentHolder.sno = (TextView) convertView.findViewById(R.id.sno);
            equivalentHolder.partNo = (TextView) convertView.findViewById(R.id.partno);
            equivalentHolder.equivalet = (TextView) convertView.findViewById(R.id.equivalent);
            convertView.setTag(equivalentHolder);
        } else {
            equivalentHolder = (EquivalentHolder) convertView.getTag();
        }

        equivalentHolder.sno.setText(new StringBuilder(String.valueOf(Integer.toString(position + 1))).append(".").toString());
        equivalentHolder.partNo.setText((CharSequence) this.originalList.get(position));
        equivalentHolder.equivalet.setText((CharSequence) this.equivalentList.get(position));
        return convertView;


    }


    class EquivalentHolder
    {
        TextView partNo,equivalet,sno;
    }


}
