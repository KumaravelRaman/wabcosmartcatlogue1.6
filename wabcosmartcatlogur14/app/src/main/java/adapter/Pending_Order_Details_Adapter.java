package adapter;

import static android.content.Context.MODE_PRIVATE;
import static com.wabco.brainmagic.wabco.catalogue.R.id.cancelbtn;
import static com.wabco.brainmagic.wabco.catalogue.R.id.sno;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.wabco.brainmagic.wabco.catalogue.R;

import alertbox.Alertbox;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import api.APIService;
import api.ApiUtils;
import models.dealer.pending.request.query.QueryPart;
import models.dealer.pending.responce.OrderDetailsResult;
import models.dealer.pending.responce.PartsDetailsResult;
import models.dealer.pending.responce.CommenResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Pending_Order_Details_Adapter extends ArrayAdapter<PartsDetailsResult> {
    private final SharedPreferences dealershare;
    private final SharedPreferences.Editor dealeredit;
    private Context context;
    private List<PartsDetailsResult> partsDetailsResult;
    private String SelectedReason;
    private OrderDetailsResult orderDetailsResult;
    private QueryPart querypart;
    private Alertbox box;

    public Pending_Order_Details_Adapter(Context context, List<PartsDetailsResult> partsDetailsResult,
                                         OrderDetailsResult orderDetailsResult) {
        super(context, R.layout.adapter_pending_order_details, partsDetailsResult);
        this.partsDetailsResult = partsDetailsResult;
        this.context = context;
        this.orderDetailsResult = orderDetailsResult;
        dealershare = context.getSharedPreferences("registration", MODE_PRIVATE);
        dealeredit = dealershare.edit();
        box = new Alertbox(context);
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final PendingOrder pendingOrderHolder;
        if (convertView == null) {
            convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater"))
                    .inflate(R.layout.adapter_pending_order_details, null);
            pendingOrderHolder = new PendingOrder();
            pendingOrderHolder.sno = (TextView) convertView.findViewById(sno);
            pendingOrderHolder.partnumber = (TextView) convertView.findViewById(R.id.partnumber);
            pendingOrderHolder.partName = (TextView) convertView.findViewById(R.id.partName);
            pendingOrderHolder.orderMrp = (TextView) convertView.findViewById(R.id.ordermrp);
            pendingOrderHolder.PartDescription =
                    (TextView) convertView.findViewById(R.id.PartDescription);
            pendingOrderHolder.ordedqty = (TextView) convertView.findViewById(R.id.ordedqty);

            pendingOrderHolder.dispatchedqty = (TextView) convertView.findViewById(R.id.dispatchedqty);
            pendingOrderHolder.status = (TextView) convertView.findViewById(R.id.status);
            pendingOrderHolder.cancelbtn = (Button) convertView.findViewById(cancelbtn);
            pendingOrderHolder.help = (TextView) convertView.findViewById(R.id.help);

            convertView.setTag(pendingOrderHolder);
        } else {
            pendingOrderHolder = (PendingOrder) convertView.getTag();
        }
        pendingOrderHolder.help.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        pendingOrderHolder.help.setTextColor(context.getResources().getColor(android.R.color.holo_blue_dark));
        pendingOrderHolder.sno.setText(Integer.toString(position + 1) + ".");
        pendingOrderHolder.help.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        pendingOrderHolder.partnumber
                .setText(partsDetailsResult.get(position).getPartNumber().toString());
        pendingOrderHolder.partName.setText(partsDetailsResult.get(position).getPartName());
        pendingOrderHolder.orderMrp.setText(context.getString(R.string.Rs) + " "
                + partsDetailsResult.get(position).getPartPrice().toString() + "0");
        pendingOrderHolder.PartDescription
                .setText(partsDetailsResult.get(position).getDescription().toString());
        pendingOrderHolder.ordedqty.setText(partsDetailsResult.get(position).getQuantity());

        if (partsDetailsResult.get(position).getDispatchedQty() == null)
            pendingOrderHolder.dispatchedqty.setText("0");
        else
            pendingOrderHolder.dispatchedqty.setText(partsDetailsResult.get(position).getDispatchedQty());

        pendingOrderHolder.status.setText(partsDetailsResult.get(position).getDeliveryStatus());

        if(dealershare.getString("usertype","").equals("Wabco employee"))
        {
            pendingOrderHolder.cancelbtn.setVisibility(View.GONE);
            pendingOrderHolder.help.setVisibility(View.GONE);
        }
        else
        {
            if (partsDetailsResult.get(position).getDeliveryStatus().equals("Fully Dispatched")) {
                pendingOrderHolder.cancelbtn.setVisibility(View.GONE);
                pendingOrderHolder.help.setVisibility(View.VISIBLE);
            }
            else if (partsDetailsResult.get(position).getDeliveryStatus().equals("Cancelled")) {
                pendingOrderHolder.cancelbtn.setVisibility(View.GONE);
                pendingOrderHolder.help.setVisibility(View.VISIBLE);
            }
            else
            {   pendingOrderHolder.cancelbtn.setVisibility(View.VISIBLE);
                pendingOrderHolder.help.setVisibility(View.GONE);
            }
        }

        pendingOrderHolder.cancelbtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                if (partsDetailsResult.get(position).isCancelled()) {
                    //partsDetailsResult.cancelbtn.setProgress(0);
                    partsDetailsResult.get(position).setCancelled(false);
                    pendingOrderHolder.cancelbtn.setText("Cancel");
                    pendingOrderHolder.cancelbtn.setTextColor(Color.DKGRAY);

                } else {
                    pendingOrderHolder.cancelbtn.setText("Selected");
                    pendingOrderHolder.cancelbtn.setTextColor(Color.RED);
                    partsDetailsResult.get(position).setCancelled(true);


                    partsDetailsResult.get(position).setDealerid(dealershare.getString("id", ""));
                    partsDetailsResult.get(position).setCancelledBy("Dealer");
                    partsDetailsResult.get(position).setDistributorid(orderDetailsResult.getDistributorid());
                    partsDetailsResult.get(position).setOrderNumber(orderDetailsResult.getOrderNumber());

                /*if (partsDetailsResult.get(position).isCancelled()) {
                    pendingOrderHolder.cancelbtn.setProgress(0);
                    partsDetailsResult.get(position).setCancelled(false);
                } else {
                    partsDetailsResult.get(position).setCancelled(true);
                    pendingOrderHolder.cancelbtn.setIndeterminateProgressMode(true); // turn on indeterminate
                    // progress
                    pendingOrderHolder.cancelbtn.setProgress(10); // set progress > 0 & < 100 to display
                    // indeterminate progress
                    pendingOrderHolder.cancelbtn.setProgress(100); // set progress to 100 or -1 to indicate
                    // complete or error state
                    partsDetailsResult.get(position).setDealerid(dealershare.getString("id", ""));
                    partsDetailsResult.get(position).setCancelledBy("Dealer");
                    partsDetailsResult.get(position).setDistributorid(orderDetailsResult.getDistributorid());
                    partsDetailsResult.get(position).setOrderNumber(orderDetailsResult.getOrderNumber());*/

                }
            }
        });


        pendingOrderHolder.help.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(context);
                View mView = layoutInflaterAndroid.inflate(R.layout.alert_input_dialog, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(context);
                alertDialogBuilderUserInput.setView(mView);

                final EditText userInputDialogEditText =
                        (EditText) mView.findViewById(R.id.userInputDialog);
                final TextView userInputDialogTitle = (TextView) mView.findViewById(R.id.dialogTitle);
                userInputDialogEditText.setHint("Enter your query");
                userInputDialogTitle.setText("Submit Query");


                alertDialogBuilderUserInput.setCancelable(false)
                        .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                // ToDo get user input here

                                if (userInputDialogEditText.getText().length() != 0) {
                                    querypart = new QueryPart();

                                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                                    Date date = new Date();
                                    System.out.println(dateFormat.format(date));

                                    querypart.setPartNumber(partsDetailsResult.get(position).getPartNumber());
                                    querypart.setPartName(partsDetailsResult.get(position).getPartName());
                                    querypart.setDescription(partsDetailsResult.get(position).getDescription());
                                    querypart.setOrderNumber(orderDetailsResult.getOrderNumber());
                                    querypart.setQuery(userInputDialogEditText.getText().toString());
                                    querypart.setQueryDate(dateFormat.format(date));


                                    SendQueryOrderedPartItem();

                                } else {
                                    StyleableToast st =
                                            new StyleableToast(context, "Enter your query !", Toast.LENGTH_SHORT);
                                    st.setBackgroundColor(context.getResources().getColor(R.color.green));
                                    st.setTextColor(Color.WHITE);
                                    st.setMaxAlpha();
                                    st.show();
                                }
                            }
                        })

                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {

                                dialogBox.cancel();
                            }
                        });

                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();


            }
        });

        return convertView;

    }


    private void SendQueryOrderedPartItem() {

        final ProgressDialog loading =
                ProgressDialog.show(context, "Sending", "Please wait...", false, false);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();

        APIService api = retrofit.create(APIService.class);
        Call<CommenResult> filtersCall;

        filtersCall = api.SendQueries(querypart);
        Log.v("Qurey part data",querypart.getQuery());

        filtersCall.enqueue(new Callback<CommenResult>() {
            @Override
            public void onResponse(Call<CommenResult> call, Response<CommenResult> response) {

                loading.dismiss();

                if (response.isSuccessful()) {
                    box.showAlertbox( response.body().getData());
                } else {
                    loading.dismiss();
                    // handle request errors yourself
                    box.showAlertbox(context.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<CommenResult> call, Throwable t) {
                loading.dismiss();
                t.printStackTrace();
                box.showAlertbox(context.getString(R.string.slow_internet_connection));

            }
        });

    }


    class PendingOrder {
        TextView partnumber, partName, PartDescription, orderMrp, ordedqty, dispatchedqty, status, sno,
                help;
        Button cancelbtn;
    }


}
