package wabco;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wabco.brainmagic.wabco.catalogue.R;
import com.wabco.brainmagic.wabco.catalogue.R.id;

public class ZoomActivity extends Activity {

	
	ImageView expandedImageView;
	TextView close;
	private String product;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_zoom);
		
		
		expandedImageView = (ImageView) findViewById(id.expanded_image);
		close = (TextView) findViewById(id.closebtn);
		
		product = getIntent().getStringExtra("ImageName");
		
		if(product.equals(""))
		{
			Picasso.with(ZoomActivity.this).load("file:///android_asset/noimagefound.jpg").into(expandedImageView);
		}
		else 
		{
			Picasso.with(ZoomActivity.this).load("file:///android_asset/"+product+".jpg").into(expandedImageView);
		}
		
		close.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			onBackPressed();	
			}
		});
		
	}

	
}
