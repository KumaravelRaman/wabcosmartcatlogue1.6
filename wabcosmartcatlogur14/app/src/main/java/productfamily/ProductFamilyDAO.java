package productfamily;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import persistence.DBHelper;


public class ProductFamilyDAO {
    private SQLiteDatabase db;
    private DBHelper dbHelper;

    public ProductFamilyDAO(Context context) {
        this.dbHelper = new DBHelper(context);
    }

    public void openDatabase() {
        this.db = this.dbHelper.readDataBase();
    }

    public void closeDatabase() {
        if (this.db != null) {
            this.db.close();
        }
    }

    public ProductFamilyDTO retrieveALL() {
        List<String> productIDList = new ArrayList<String>();
        List<String> productNameList = new ArrayList<String>();
        ProductFamilyDTO vehicleDAO = new ProductFamilyDTO();
        openDatabase();
        Cursor cursor = this.db.rawQuery("Select distinct Vehicle_Product_id, ProducutName from product order by ProducutName asc", null);
        if (cursor.moveToFirst()) {
            do {
                productNameList.add(cursor.getString(cursor.getColumnIndex("ProducutName")));
                productIDList.add(cursor.getString(cursor.getColumnIndex("Vehicle_Product_id")));
            } while (cursor.moveToNext());
        }
        closeDatabase();
        vehicleDAO.setProductNameList(productNameList);
        vehicleDAO.setProductIDList(productIDList);
        return vehicleDAO;
    }

    public ProductFamilyDTO retrieveALLForAll(String ProductName) {
    	Log.v("product id", ProductName);
        List<String> vehicleNameList = new ArrayList<String>();
        List<String> vehicleIDList = new ArrayList<String>();
        List<String> productPartno_List = new ArrayList<String>();
        List<String> Description_List = new ArrayList<String>();
        List<String> image_List = new ArrayList<String>();
        List<String> productIDList = new ArrayList<String>();
        List<String> Flag_List = new ArrayList<String>();
        ProductFamilyDTO vehicleDAO = new ProductFamilyDTO();
        openDatabase();
        Log.v("Query === ","select distinct v.Vehicle_name,v.Vehicle_id,p.Part_No,p.Description,p.ProductImage,p.flag from products_assemply p,vehicle_make v ,product a where v.Vehicle_id = p.Vehicle_id and p.Vehicle_Product_id =a.Vehicle_Product_id and p.Vehicle_Product_id ='" + ProductName + "'");
        Cursor cursor = this.db.rawQuery("select distinct v.Vehicle_name,v.Vehicle_id,p.Part_No,p.Description,p.ProductImage,p.flag,p.Vehicle_Product_id from products_assemply p,vehicle_make v ,product a where v.Vehicle_id = p.Vehicle_id and p.Vehicle_Product_id =a.Vehicle_Product_id and p.Vehicle_Product_id ='" + ProductName + "'", null);
        if (cursor.moveToFirst()) 
        {
            do 
            {
                vehicleNameList.add(cursor.getString(cursor.getColumnIndex("Vehicle_name")));
                productPartno_List.add(cursor.getString(cursor.getColumnIndex("Part_No")));
                Description_List.add(cursor.getString(cursor.getColumnIndex("Description")));
                image_List.add(cursor.getString(cursor.getColumnIndex("ProductImage")));
                Flag_List.add(SetFlag(cursor.getString(cursor.getColumnIndex("Part_No"))));
                vehicleIDList.add(cursor.getString(cursor.getColumnIndex("Vehicle_id")));
                productIDList.add(cursor.getString(cursor.getColumnIndex("Vehicle_Product_id")));
                
                Log.v(cursor.getString(cursor.getColumnIndex("Vehicle_name")), 
                		cursor.getString(cursor.getColumnIndex("Vehicle_id")));
                
            } while (cursor.moveToNext());
        }
        closeDatabase();
        vehicleDAO.setVehicleNameList(vehicleNameList);
        vehicleDAO.setPart_No_List(productPartno_List);
        vehicleDAO.setDescription(Description_List);
        vehicleDAO.setService_Part(image_List);
        vehicleDAO.setFlag_List(Flag_List);
        vehicleDAO.setVehicleIDList(vehicleIDList);
        vehicleDAO.setProductIDList(productIDList);
        return vehicleDAO;
    }
    
    
    private String SetFlag(String partno)
    {
		// TODO Auto-generated method stub
         openDatabase();

        String query ="select Part_No from pricelist where  Part_No ='" + partno + "' or Partcode  ='" + partno + "'";


        Cursor cursor = this.db.rawQuery(query, null);
         if (cursor.moveToFirst()) 
         {
             return "0";
         }
         else 
         {
        	  return "1";
		 }
      
	}

    public ProductFamilyDTO retrieveVehicleName() {
        List<String> vehicleNameList = new ArrayList<String>();
        ProductFamilyDTO vehicleDAO = new ProductFamilyDTO();
        openDatabase();
        Cursor cursor = this.db.rawQuery("select  Vehicle_name from vehicle_make", null);
        if (cursor.moveToFirst()) {
            do {
                vehicleNameList.add(cursor.getString(cursor.getColumnIndex("Vehicle_name")));
            } while (cursor.moveToNext());
        }
        closeDatabase();
        vehicleDAO.setVehicleForSpinnerNameList(vehicleNameList);
        return vehicleDAO;
    }

    public ProductFamilyDTO retriveForVehicle(String item, String ProductName) {
        List<String> productNameList = new ArrayList<String>();
        List<String> vehicleNameList = new ArrayList<String>();
        List<String> productPartno_List = new ArrayList<String>();
        List<String> Description_List = new ArrayList<String>();
        List<String> image_List = new ArrayList<String>();
        List<String> vehicleIDList = new ArrayList<String>();
        List<String> Flag_List = new ArrayList<String>();
        ProductFamilyDTO vehicleDAO = new ProductFamilyDTO();
        openDatabase();
        
        Log.v("Query for select", "select  distinct v.Vehicle_name,v.Vehicle_id,p.Part_No,p.Description,p.ProductImage from products_assemply p,vehicle_make v ,product a where v.Vehicle_id = p.Vehicle_id and p.Vehicle_Product_id ='" + ProductName + "'and v.Vehicle_id='" + item + "'and p.Vehicle_Product_id =a.Vehicle_Product_id");
        
        
        Cursor cursor = this.db.rawQuery("select distinct v.Vehicle_name,v.Vehicle_id,p.Part_No,p.Description,p.ProductImage from products_assemply p,vehicle_make v ,product a where v.Vehicle_id = p.Vehicle_id and p.Vehicle_Product_id ='" + ProductName + "'and v.Vehicle_id='" + item + "'and p.Vehicle_Product_id =a.Vehicle_Product_id", null);
        if (cursor.moveToFirst()) {
            do {
            	vehicleIDList.add(cursor.getString(cursor.getColumnIndex("Vehicle_id")));
                vehicleNameList.add(cursor.getString(cursor.getColumnIndex("Vehicle_name")));
                productPartno_List.add(cursor.getString(cursor.getColumnIndex("Part_No")));
                Description_List.add(cursor.getString(cursor.getColumnIndex("Description")));
                image_List.add(cursor.getString(cursor.getColumnIndex("ProductImage")));
                Flag_List.add(SetFlag(cursor.getString(cursor.getColumnIndex("Part_No"))));
            } while (cursor.moveToNext());
        }
        closeDatabase();
        vehicleDAO.setProductNameList(productNameList);
        vehicleDAO.setVehicleNameList(vehicleNameList);
        vehicleDAO.setPart_No_List(productPartno_List);
        vehicleDAO.setDescription(Description_List);
        vehicleDAO.setService_Part(image_List);
        vehicleDAO.setVehicleIDList(vehicleIDList);
        vehicleDAO.setFlag_List(Flag_List);
        return vehicleDAO;
    }

    public ProductFamilyDTO retriveForVehicleForshowAll(String item) {
        List<String> productNameList = new ArrayList<String>();
        List<String> vehicleNameList = new ArrayList<String>();
        List<String> productPartno_List = new ArrayList<String>();
        List<String> Description_List = new ArrayList<String>();
        List<String> image_List = new ArrayList<String>();
        List<String> Flag_List = new ArrayList<String>();
        ProductFamilyDTO vehicleDAO = new ProductFamilyDTO();
        openDatabase();
        Cursor cursor = this.db.rawQuery("select distinct v.Vehicle_name, a.Part_No,a.Description,p.ProductImage from products_assemply a,product p,vehicle_make v where  v.Vehicle_id =a.Vehicle_id  and v.Vehicle_id=" + item + "'", null);
        if (cursor.moveToFirst()) {
            do {
                vehicleNameList.add(cursor.getString(cursor.getColumnIndex("Vehicle_name")));
                productPartno_List.add(cursor.getString(cursor.getColumnIndex("Part_No")));
                Description_List.add(cursor.getString(cursor.getColumnIndex("Description")));
                image_List.add(cursor.getString(cursor.getColumnIndex("ProductImage")));
                Flag_List.add(SetFlag(cursor.getString(cursor.getColumnIndex("Part_No"))));
            } while (cursor.moveToNext());
        }
        closeDatabase();
        vehicleDAO.setProductNameList(productNameList);
        vehicleDAO.setVehicleNameList(vehicleNameList);
        vehicleDAO.setPart_No_List(productPartno_List);
        vehicleDAO.setDescription(Description_List);
        vehicleDAO.setService_Part(image_List);
        vehicleDAO.setFlag_List(Flag_List);
        return vehicleDAO;
    }

    public ProductFamilyDTO retrieveALLForShowAll(String item) {
        List<String> vehicleNameList = new ArrayList<String>();
        List<String> productPartno_List = new ArrayList<String>();
        List<String> Description_List = new ArrayList<String>();
        List<String> image_List = new ArrayList<String>();
        List<String> Flag_List = new ArrayList<String>();
        ProductFamilyDTO vehicleDAO = new ProductFamilyDTO();
        openDatabase();
        Cursor cursor = this.db.rawQuery("select distinct v.Vehicle_name,p.Part_No,p.Description,a.ProductImage,p.Vehicle_Product_id from products_assemply p,product a,vehicle_make v where v.Vehicle_id = p.Vehicle_id   and p.Vehicle_Product_id='" + item + "'", null);
        if (cursor.moveToFirst()) {
            do {
                vehicleNameList.add(cursor.getString(cursor.getColumnIndex("Vehicle_name")));
                productPartno_List.add(cursor.getString(cursor.getColumnIndex("Part_No")));
                Description_List.add(cursor.getString(cursor.getColumnIndex("Description")));
                image_List.add(cursor.getString(cursor.getColumnIndex("ProductImage")));
                Flag_List.add(SetFlag(cursor.getString(cursor.getColumnIndex("Part_No"))));
            } while (cursor.moveToNext());
        }
        closeDatabase();
        vehicleDAO.setVehicleNameList(vehicleNameList);
        vehicleDAO.setPart_No_List(productPartno_List);
        vehicleDAO.setDescription(Description_List);
        vehicleDAO.setService_Part(image_List);
        vehicleDAO.setFlag_List(Flag_List);
        return vehicleDAO;
    }
}
