package alertbox;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class Alertbox {
	Context context;
	
	public Alertbox(Context con) {
		// TODO Auto-generated constructor stub
		this.context = con;
	}
	

	public void showAlertbox(String msg)
	{
		final AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				context);
		alertDialog.setMessage(msg);
		alertDialog.setTitle("WABCO");
		alertDialog.setCancelable(false);
		alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

			}
		});
		//alertDialog.setIcon(R.drawable.logo);
		alertDialog.show();
	}
	
	

	public void showNegativebox(String msg)
	{
		final AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				context);
		alertDialog.setMessage(msg);
		alertDialog.setTitle("WABCO");
		alertDialog.setCancelable(false);
		alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// TODO Auto-generated method stub

				((Activity) context).onBackPressed();
			}
		});
		alertDialog.show();
	}

	public void showGenunieCheckBox(String msg)
	{
		final AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				context);
		alertDialog.setMessage(msg);
		alertDialog.setTitle("WABCO Geniune");
		alertDialog.setCancelable(false);
		alertDialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// TODO Auto-generated method stub


			}
		});

		alertDialog.show();
	}
	
}
