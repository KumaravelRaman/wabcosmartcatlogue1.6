
package models.dealer.pending.responce;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CancelSingleOrderResult {

    @SerializedName("data")
    private String mData;
    @SerializedName("result")
    private String mResult;

    public String getData() {
        return mData;
    }

    public void setData(String data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
