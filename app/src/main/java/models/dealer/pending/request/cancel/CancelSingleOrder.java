
package models.dealer.pending.request.cancel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class CancelSingleOrder {

    @SerializedName("Cancelby_Orders")
    private List<CancelbyOrder> mCancelbyOrders;

    public List<CancelbyOrder> getCancelbyOrders() {
        return mCancelbyOrders;
    }

    public void setCancelbyOrders(List<CancelbyOrder> CancelbyOrders) {
        mCancelbyOrders = CancelbyOrders;
    }

}
