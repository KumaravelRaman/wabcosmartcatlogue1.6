
package models.dealer.pending.request.cancel;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CancelPart {

    @SerializedName("CancelledBy")
    private String mCancelledBy;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("OrderNumber")
    private String mOrderNumber;
    @SerializedName("PartName")
    private String mPartName;
    @SerializedName("PartNumber")
    private String mPartNumber;
    @SerializedName("PartPrice")
    private String mPartPrice;
    @SerializedName("Quantity")
    private String mQuantity;
    @SerializedName("Reason")
    private String mReason;
    @SerializedName("TotalAmount")
    private String mTotalAmount;
    @SerializedName("UpdatedDate")
    private String mUpdatedDate;

    @SerializedName("Active")
    private String Active;

    public String getCancelledBy() {
        return mCancelledBy;
    }

    public void setCancelledBy(String CancelledBy) {
        mCancelledBy = CancelledBy;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String Description) {
        mDescription = Description;
    }

    public String getOrderNumber() {
        return mOrderNumber;
    }

    public void setOrderNumber(String OrderNumber) {
        mOrderNumber = OrderNumber;
    }

    public String getPartName() {
        return mPartName;
    }

    public void setPartName(String PartName) {
        mPartName = PartName;
    }

    public String getPartNumber() {
        return mPartNumber;
    }

    public void setPartNumber(String PartNumber) {
        mPartNumber = PartNumber;
    }

    public String getPartPrice() {
        return mPartPrice;
    }

    public void setPartPrice(String PartPrice) {
        mPartPrice = PartPrice;
    }

    public String getQuantity() {
        return mQuantity;
    }

    public void setQuantity(String Quantity) {
        mQuantity = Quantity;
    }

    public String getReason() {
        return mReason;
    }

    public void setReason(String Reason) {
        mReason = Reason;
    }

    public String getTotalAmount() {
        return mTotalAmount;
    }

    public void setTotalAmount(String TotalAmount) {
        mTotalAmount = TotalAmount;
    }

    public String getUpdatedDate() {
        return mUpdatedDate;
    }

    public void setUpdatedDate(String UpdatedDate) {
        mUpdatedDate = UpdatedDate;
    }

    public String getActive() {
        return Active;
    }

    public void setActive(String active) {
        Active = active;
    }

}
