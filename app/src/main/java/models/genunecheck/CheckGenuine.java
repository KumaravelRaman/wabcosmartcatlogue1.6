
package models.genunecheck;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CheckGenuine {

    @SerializedName("data")
    private List<GenuineLoglist> mData;
    @SerializedName("result")
    private String mResult;

    public List<GenuineLoglist> getData() {
        return mData;
    }

    public void setData(List<GenuineLoglist> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
