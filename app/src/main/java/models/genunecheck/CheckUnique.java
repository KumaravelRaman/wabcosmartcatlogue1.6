
package models.genunecheck;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CheckUnique {

    @SerializedName("data")
    private CheckUniquelist mCheckUniquelist;
    @SerializedName("result")
    private String mResult;

    public CheckUniquelist getData() {
        return mCheckUniquelist;
    }

    public void setData(CheckUniquelist checkUniquelist) {
        mCheckUniquelist = checkUniquelist;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
