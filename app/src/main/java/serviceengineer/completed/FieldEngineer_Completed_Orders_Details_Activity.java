package serviceengineer.completed;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import adapter.Completed_Order_Details_Adapter;
import adapter.Pending_Order_Details_Adapter;
import alertbox.Alertbox;
import api.APIService;
import api.ApiUtils;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import models.dealer.pending.responce.OrderDetailsResult;
import models.dealer.pending.responce.PartsDetails;
import models.dealer.pending.responce.PartsDetailsResult;
import network.NetworkConnection;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import search.SearchActivity;
import serviceengineer.account.FieldEngineer_Account;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;


public class FieldEngineer_Completed_Orders_Details_Activity extends Activity {

    private ImageView Backbtn,cart_icon;
    private View heade_Layout;
    private TextView Tittle;
    private Button Cancelbtn;
    private OrderDetailsResult orderDetailsResult;
    private ImageView Accountbtn;
    private TextView OrderNumberTxt, DistributorTxt, TotalMRPTxt, OrderDateTxt;
    private ListView listView;
    private Alertbox box = new Alertbox(FieldEngineer_Completed_Orders_Details_Activity.this);
    private SharedPreferences dealershare;
    private SharedPreferences.Editor dealeredit;
    private Button Updatebtn;
    private MaterialSpinner Partnumber_filter, Status_filter;
    private String SelectedPartNumber, SelectedPartStatus;
    private Completed_Order_Details_Adapter adapter;
    private List<PartsDetailsResult> partsDetailsResults, partsDetails;
    private ImageView Menubtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_field_engineer__completed__orders__details_);
        dealershare = getSharedPreferences("registration", MODE_PRIVATE);
        dealeredit = dealershare.edit();

        heade_Layout =  findViewById(R.id.header_layout);
        Tittle = (TextView) heade_Layout.findViewById(R.id.tittle);
        Backbtn = (ImageView) heade_Layout.findViewById(R.id.back);
        cart_icon = (ImageView) heade_Layout.findViewById(R.id.cart_icon);
        listView = (ListView) findViewById(R.id.listView);
        Accountbtn = (ImageView) heade_Layout.findViewById(R.id.account_icon);
        cart_icon.setVisibility(View.GONE);
        Tittle.setText("Completed Orders Details");

        Partnumber_filter = (MaterialSpinner) findViewById(R.id.partnumber_filter);
        Status_filter = (MaterialSpinner) findViewById(R.id.status_filter);

        Partnumber_filter.setBackgroundResource(R.drawable.autotextback);
        Status_filter.setBackgroundResource(R.drawable.autotextback);



        Updatebtn = (Button) findViewById(R.id.update);
        listView = (ListView) findViewById(R.id.listView);

        OrderNumberTxt = (TextView) findViewById(R.id.order_number);
        DistributorTxt = (TextView) findViewById(R.id.distributor_name);
        TotalMRPTxt = (TextView) findViewById(R.id.totalMrp);
        OrderDateTxt = (TextView) findViewById(R.id.orderdate);


        orderDetailsResult = (OrderDetailsResult) getIntent().getSerializableExtra("orderObject");
        DistributorTxt.setText(orderDetailsResult.getDistrName());
        OrderNumberTxt.setText(orderDetailsResult.getOrderNumber());
        TotalMRPTxt.setText(getResources().getString(R.string.Rs) + " "
                + orderDetailsResult.getTotalOrderAmount() + "0");


        String[] splitdate = orderDetailsResult.getOrderedDate().split("T");
        OrderDateTxt.setText(splitdate[0]);

        Partnumber_filter.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                SelectedPartNumber = item.toString();

                if (!SelectedPartNumber.equals("All")) {
                    List<PartsDetailsResult> resultList = new ArrayList<PartsDetailsResult>();
                    for (PartsDetailsResult P : partsDetails) {
                        if (P.getPartNumber().equals(SelectedPartNumber)) {
                            resultList.add(P);
                            adapter.notifyDataSetChanged();
                            listView.setAdapter(new Pending_Order_Details_Adapter(
                                    FieldEngineer_Completed_Orders_Details_Activity.this, resultList, orderDetailsResult));
                        }
                    }
                    Status_filter.setSelectedIndex(1);
                } else {
                    adapter = new Completed_Order_Details_Adapter(FieldEngineer_Completed_Orders_Details_Activity.this,
                            partsDetailsResults, orderDetailsResult);
                    listView.setAdapter(adapter);
                }
            }
        });

        Status_filter.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                SelectedPartStatus = item.toString();
                if (!SelectedPartStatus.equals("All")) {
                    List<PartsDetailsResult> resultList = new ArrayList<PartsDetailsResult>();
                    for (PartsDetailsResult P : partsDetails) {

                        if (P.getDeliveryStatus().equals(SelectedPartStatus)) {
                            resultList.add(P);
                            adapter.notifyDataSetChanged();
                            listView.setAdapter(new Pending_Order_Details_Adapter(
                                    FieldEngineer_Completed_Orders_Details_Activity.this, resultList, orderDetailsResult));
                        }
                    }
                    Partnumber_filter.setSelectedIndex(1);
                } else {
                    adapter = new Completed_Order_Details_Adapter(FieldEngineer_Completed_Orders_Details_Activity.this,
                            partsDetailsResults, orderDetailsResult);
                    listView.setAdapter(adapter);
                }

            }
        });


        Accountbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(
                        new Intent(FieldEngineer_Completed_Orders_Details_Activity.this, FieldEngineer_Account.class));
            }
        });


        Backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Menubtn = (ImageView) heade_Layout.findViewById(R.id.menu);
        Menubtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(FieldEngineer_Completed_Orders_Details_Activity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(FieldEngineer_Completed_Orders_Details_Activity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(FieldEngineer_Completed_Orders_Details_Activity.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(FieldEngineer_Completed_Orders_Details_Activity.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(FieldEngineer_Completed_Orders_Details_Activity.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(FieldEngineer_Completed_Orders_Details_Activity.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(FieldEngineer_Completed_Orders_Details_Activity.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(FieldEngineer_Completed_Orders_Details_Activity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(FieldEngineer_Completed_Orders_Details_Activity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(FieldEngineer_Completed_Orders_Details_Activity.this, PriceListActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(FieldEngineer_Completed_Orders_Details_Activity.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });


        CheckInternet();

    }

    private void CheckInternet() {

        if (new NetworkConnection(FieldEngineer_Completed_Orders_Details_Activity.this).CheckInternet()) {
            GetOrderedData();
        } else {
            box.showAlertbox(getResources().getString(R.string.nointernetmsg));
        }
    }

    private void GetOrderedData() {
        final ProgressDialog loading = ProgressDialog.show(FieldEngineer_Completed_Orders_Details_Activity.this,
                "Loading", "Please wait", false, false);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();
        APIService api = retrofit.create(APIService.class);
        Call<PartsDetails> filtersCall = api.GetCompletedOrderDetailsForFeildEngineer(dealershare.getString("id", ""),
                orderDetailsResult.getOrderNumber());
        filtersCall.enqueue(new Callback<PartsDetails>() {

            @Override
            public void onResponse(Call<PartsDetails> call, Response<PartsDetails> response) {

                loading.dismiss();

                if (response.isSuccessful()) {
                    response.body().getResult().toString();
                    partsDetailsResults = response.body().getData();
                    partsDetails = response.body().getData();
                    LoadListviewData(response.body().getData());
                } else {
                    loading.dismiss(); // handle request errors yourself
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<PartsDetails> call, Throwable t) {
                loading.dismiss();
                t.printStackTrace();
                box.showAlertbox(getResources().getString(R.string.server_error));

            }
        });
    }

    private void LoadListviewData(List<PartsDetailsResult> result) {

        adapter = new Completed_Order_Details_Adapter(FieldEngineer_Completed_Orders_Details_Activity.this, result,
                orderDetailsResult);

        listView.setAdapter(adapter);
        List<String> partnumber = new ArrayList();
        List<String> partstatus = new ArrayList();
        partnumber.add("All");
        partstatus.add("All");
        partnumber.add("Select Part");
        partstatus.add("Select Status");
        for (int i = 0; i < result.size(); i++) {

            partnumber.add(partsDetailsResults.get(i).getPartNumber());
            Partnumber_filter.setItems(partnumber);

            partstatus.add(partsDetailsResults.get(i).getDeliveryStatus());
            partstatus = new ArrayList<String>(new LinkedHashSet<String>(partstatus));
            Status_filter.setItems(partstatus);

        }
    }

}
