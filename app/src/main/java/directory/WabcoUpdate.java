package directory;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import alertbox.Alertbox;
import network.NetworkConnection;
import registration.ServerConnection;

public class WabcoUpdate {
	
	boolean vesionstatus;
	private Context condext;
	public ProgressDialog loadDialog;
int versiontxt;
public ServerConnection server;
public Statement stmt;
private Connection connection;
public ResultSet rset;

public int localversion;
private SharedPreferences myshare;
private String urll;
public WabcoUpdate(Context context)
	{
		this.condext = context;
		 urll ="http://demo.brainmagic.info/wabco/Wabco.apk";
	}
//ftp://brainmagic@103.21.58.247/demo.brainmagic.info/wabco/Wabco.apk
	
	
	public boolean checkVersion() {
		// TODO Auto-generated method stub
		myshare = condext.getSharedPreferences("registration", 0);
		myshare.edit();
		localversion =	myshare.getInt("VERSION", 0);
		Log.v("local version====", Integer.toString(localversion));
		NetworkConnection net = new NetworkConnection(condext);
		if(net.CheckInternet())
		{
			new CheckVersion().execute();

		}
		else 
		{
			showAlertBox("Not able to connect please check your network connection and try again.");
			
		}
		
		
		
		
		return vesionstatus;
	}
	
	class CheckVersion extends AsyncTask<String, Void, String>
	{
		

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			loadDialog = new ProgressDialog(condext);
			loadDialog.setMessage("Loading...");
			loadDialog.setProgressStyle(0);
			loadDialog.setCancelable(false);
			loadDialog.show();
		}
		@Override
		protected String doInBackground(String... params)
		{
			// TODO Auto-generated method stub
			try
			{
			 server = new ServerConnection();
			 connection = server.getConnection(); 
			 stmt = connection.createStatement();
			 rset = stmt.executeQuery("select version from version");
			while(rset.next())
			{
				versiontxt = rset.getInt("VERSION");
			}

			rset.close();
			stmt.close();
			connection.close();
			return "success";
			
			}
			catch(Exception m)
			{
				return "error";
			}
			
		
			
		}
		
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			loadDialog.dismiss();
			if(result.equals("success"))
			{
				if(localversion>=versiontxt)
				{
					showAlertBox("WABCO is Up to Date");
				}
				else 
				{

					AlertDialog.Builder alertDialog = new AlertDialog.Builder(condext);
					alertDialog.setTitle("WABCO");
					alertDialog.setMessage("WABCO Smart Catalogue updated Application available on Play Store \nDo you want to download?");
					alertDialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
							final String appPackageName = condext.getPackageName(); // getPackageName() from Context or Activity object
							try {
								condext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" +condext.getPackageName())));
							} catch (android.content.ActivityNotFoundException anfe) {
								condext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
							}
							vesionstatus = false;
						}
					});
					alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();

						}
					});
					alertDialog.show();

					
				}
				
			}
			else {
				showAlertBox("Something Went Wrong Please try again.");
			}
		}
	}
	
	
	public void Update(String apkurl){
	    try {
	        URL url = new URL(apkurl);
	        HttpURLConnection c = (HttpURLConnection) url.openConnection();
	        c.setRequestMethod("GET");
	        c.setDoOutput(true);
	        c.connect();

	    	
	    	
	        String PATH = Environment.getExternalStorageDirectory() + "/download/";
	        File file = new File(PATH);
	        file.mkdirs();
	        File outputFile = new File(file, "Wabco.apk");
	        FileOutputStream fos = new FileOutputStream(outputFile);

	        InputStream is = c.getInputStream();

	        byte[] buffer = new byte[1024];
	        int len1 = 0;
	        while ((len1 = is.read(buffer)) != -1) {
	            fos.write(buffer, 0, len1);
	        }
	        fos.close();
	        is.close();//till here, it works fine - .apk is download to my sdcard in download file

	        Intent promptInstall = new Intent(Intent.ACTION_VIEW)
	            .setData(Uri.parse(PATH+"app.apk"))
	            .setType("application/android.com.app");
	        condext.startActivity(promptInstall);//installation is not working

	    } catch (IOException e) {
	    	
	    	e.printStackTrace();
	        Toast.makeText(condext, "Update error!", Toast.LENGTH_LONG).show();
	    }
	}  
	
	
	private void showAlertBox(String msg) 
	{
		// TODO Auto-generated method stub
		Alertbox box = new Alertbox(condext);
		box.showAlertbox(msg);
	}
	
	
	private void negativeAlertBox(String msg) 
	{
		// TODO Auto-generated method stub
		Alertbox box = new Alertbox(condext);
		box.showNegativebox(msg);
	}
	
	
	
	
}
