package productfamily;

import java.util.List;

import com.wabco.brainmagic.wabco.catalogue.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

@SuppressLint({"InflateParams"})
public class ProductFamilyAdapter extends ArrayAdapter<String> {
    private Context context;
    private List<String> itemTextList;

    public ProductFamilyAdapter(Context context, List<String> itemTextList) {
        super(context, R.layout.activity_productfamily_text_item, itemTextList);
        this.context = context;
        this.itemTextList = itemTextList;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        VehicleMakeHolder vehicleMakeHolder;
        if (convertView == null) {
            convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater"))
            		.inflate(R.layout.activity_productfamily_text_item, null);
            vehicleMakeHolder = new VehicleMakeHolder();
            vehicleMakeHolder.textItem = (TextView) convertView.findViewById(R.id.item_text);
            convertView.setTag(vehicleMakeHolder);
        } else {
            vehicleMakeHolder = (VehicleMakeHolder) convertView.getTag();
        }
        vehicleMakeHolder.textItem.setText((CharSequence) this.itemTextList.get(position));
        return convertView;
    }
}
