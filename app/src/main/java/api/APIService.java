package api;

import models.asc.ASCDetails;
import models.dealer.pending.request.cancel.CancelOrder;
import models.dealer.pending.request.cancel.CancelSingleOrder;
import models.dealer.pending.request.query.QueryOrder;
import models.dealer.pending.request.query.QueryPart;
import models.dealer.pending.responce.CancelSingleOrderResult;
import models.dealer.pending.responce.CommenResult;
import models.dealer.pending.responce.OrderDetails;
import models.dealer.pending.responce.PartsDetails;
import models.distributor.DistributorModel;
import models.genunecheck.CheckGenuine;
import models.genunecheck.CheckUnique;
import models.genunecheck.SerialList;
import models.genunecheck.UpdateUser;
import models.genunecheck.WabcoEmployee;
import models.loginmodel.Login;
import models.quickorder.request.QuickOrder;
import models.quickorder.response.OrderResponse;
import models.usermodel.User;
import models.whatsnew.WhatsNewView;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;


/**
 * Created by system01 on 5/11/2017.
 */

public interface APIService {


    public static final String BASE_URL = "https://genuinecheck.wabcoindia-am.com/gueninecheck/"; //new brainmagic url//current url
//    public static final String BASE_URL = "https://genuinechecktest.wabcoindia-am.com/gen/"; // wabco server


    //Post Registered User
    @FormUrlEncoded
    @POST("api/MOM/registeration")
    public Call<User> InsetRegistration(
            @Field("name") String name,
            @Field("email") String email,
            @Field("phone") String mobileno,
            @Field("usertype") String usertype,
            @Field("city") String city,
            @Field("state") String state,
            @Field("Pincode") String pincode,
            @Field("country") String country,
            @Field("date") String date,
            @Field("deviceid") String deviceid,
            @Field("shopname") String shopname,
            @Field("address") String address,
            @Field("dealercode") String dealercode,
            @Field("gstnumber") String gstnumber,
            @Field("pannumber") String pannumber,
            @Field("DeviceType") String DeviceType);


    //Post Registered User
    @FormUrlEncoded
    @POST("api/MOM/registeration")
    public Call<User> UpdateUser(
            @Field("id") String id,
            @Field("name") String name,
            @Field("email") String email,
            @Field("phone") String mobileno,
            @Field("usertype") String usertype,
            @Field("city") String city,
            @Field("Pincode") String pincode,
            @Field("state") String state,
            @Field("country") String country,
            @Field("date") String date,
            @Field("deviceid") String deviceid,
            @Field("shopname") String shopname,
            @Field("address") String address,
            @Field("username") String username,
            @Field("password") String password,
            @Field("dealercode") String dealercode,
            @Field("gstnumber") String gstnumber,
            @Field("pannumber") String pannumber);


    //Post Registered User
    @FormUrlEncoded
    @POST("api/MOM/createlogin")
    public Call<User> CreateLogin(
            @Field("id") String id,
            @Field("name") String name,
            @Field("email") String email,
            @Field("phone") String mobileno,
            @Field("usertype") String usertype,
            @Field("city") String city,
            @Field("Pincode") String pincode,
            @Field("state") String state,
            @Field("country") String country,
            @Field("date") String date,
            @Field("deviceid") String deviceid,
            @Field("shopname") String shopname,
            @Field("address") String address,
            @Field("username") String username,
            @Field("password") String password,
            @Field("gstnumber") String gstnumber,
            @Field("pannumber") String pannumber);


    // check login User
    @FormUrlEncoded
    @POST("api/MOM/CheckUser")
    public Call<CommenResult> CheckUserLogin(
            @Field("id") String id,
            @Field("username") String username,
            @Field("password") String password,
            @Field("usertype") String usertype);


    //chek user for wabco empoyee
    // check login User
    @FormUrlEncoded
    @POST("api/MOM/fsrlogin")
    public Call<CommenResult> CheckUserLoginWabcoempoyee(
            @Field("id") String id,
            @Field("username") String username,
            @Field("password") String password,
            @Field("email") String email);


    // Get Distributor

    @GET("api/MOM/getAllDistributors")
    public Call<DistributorModel> getALLDistributors();


    // Get Distributor by state and city
    @FormUrlEncoded
    @POST("api/MOM/getDistributors")
    public Call<DistributorModel> getDistributors(
            @Field("State") String State,
            @Field("City") String City);


    @POST("api/MOM/SaveOrders")
    public Call<OrderResponse> PostOrderDetails(
            @Body QuickOrder quickOrder);

    // Pending orders service methods

    @FormUrlEncoded
    @POST("api/MOM/searchpending")
    public Call<OrderDetails> GetPendingOrderFilters(@Field("Dealerid") String id);

    @FormUrlEncoded
    @POST("api/MOM/pendingfill")
    public Call<OrderDetails> GetPendingOrderDetailsForName(
            @Field("Dealerid") String Dealerid,
            @Field("FilterType") String FilterType,
            @Field("DistrName") String DistrName,
            @Field("Distributorid") String Distributorid);


    @FormUrlEncoded
    @POST("api/MOM/pendingfill")
    public Call<OrderDetails> GetPendingOrderDetailsForOrderNumber(
            @Field("Dealerid") String Dealerid,
            @Field("FilterType") String FilterType,
            @Field("OrderNumber") String OrderNumber);


    @FormUrlEncoded
    @POST("api/MOM/pendingfill")
    public Call<OrderDetails> GetPendingOrderDetailsForOrderDate(
            @Field("Dealerid") String Dealerid,
            @Field("FilterType") String FilterType,
            @Field("OrderDate") String OrderDate);


    @FormUrlEncoded
    @POST("api/MOM/porderdetails")
    public Call<PartsDetails> GetPendingOrderDetails(
            @Field("Dealerid") String id,
            @Field("OrderNumber") String orderNumber);


    @POST("api/MOM/CancelOrders")
    public Call<CancelSingleOrderResult> SendCancelPart(
            @Body CancelOrder cancelOrder);

    @POST("api/MOM/CancelSingle")
    public Call<CommenResult> CancelSingleOrder(
            @Body CancelSingleOrder cancelOrder);


    @POST("api/MOM/QueryParts")
    public Call<CommenResult> SendQueries(
            @Body QueryPart query);


    @POST("api/MOM/Query")
    public Call<CommenResult> SendQueriesForOrder(
            @Body QueryOrder query);

// Completed orders service methods

    @FormUrlEncoded
    @POST("api/MOM/searchprocessed")
    public Call<OrderDetails> GetCompletedOrderFilters(@Field("Dealerid") String id);

    @FormUrlEncoded
    @POST("api/MOM/processedfill")
    public Call<OrderDetails> GetCompletedOrderDetailsForName(
            @Field("Dealerid") String Dealerid,
            @Field("FilterType") String FilterType,
            @Field("DistrName") String DistrName,
            @Field("Distributorid") String Distributorid);


    @FormUrlEncoded
    @POST("api/MOM/processedfill")
    public Call<OrderDetails> GetCompletedOrderDetailsForOrderNumber(
            @Field("Dealerid") String Dealerid,
            @Field("FilterType") String FilterType,
            @Field("OrderNumber") String OrderNumber);


    @FormUrlEncoded
    @POST("api/MOM/processedfill")
    public Call<OrderDetails> GetCompletedOrderDetailsForOrderDate(
            @Field("Dealerid") String Dealerid,
            @Field("FilterType") String FilterType,
            @Field("OrderDate") String OrderDate);


    @FormUrlEncoded
    @POST("api/MOM/porderdetails1")
    public Call<PartsDetails> GetCompletedOrderDetails(
            @Field("Dealerid") String id,
            @Field("OrderNumber") String orderNumber);


// Field Engineer Account pending and completed orders services


    // Pending orders service methods

    @FormUrlEncoded
    @POST("api/MOM/fsearchpending")
    public Call<OrderDetails> GetPendingOrderFiltersForFeildEngineer(@Field("Dealerid") String id);

    @FormUrlEncoded
    @POST("api/MOM/fpendingfill")
    public Call<OrderDetails> GetPendingOrderDetailsForNameForFeildEngineer(
            @Field("Dealerid") String Dealerid,
            @Field("FilterType") String FilterType,
            @Field("DistrName") String DistrName,
            @Field("Distributorid") String Distributorid);


    @FormUrlEncoded
    @POST("api/MOM/fpendingfill")
    public Call<OrderDetails> GetPendingOrderDetailsForOrderNumberForFeildEngineer(
            @Field("Dealerid") String Dealerid,
            @Field("FilterType") String FilterType,
            @Field("OrderNumber") String OrderNumber);


    @FormUrlEncoded
    @POST("api/MOM/fpendingfill")
    public Call<OrderDetails> GetPendingOrderDetailsForOrderDateForFeildEngineer(
            @Field("Dealerid") String Dealerid,
            @Field("FilterType") String FilterType,
            @Field("OrderDate") String OrderDate);


    @FormUrlEncoded
    @POST("api/MOM/fporderdetails")
    public Call<PartsDetails> GetPendingOrderDetailsForFeildEngineer(
            @Field("Dealerid") String id,
            @Field("OrderNumber") String orderNumber);


// Completed orders service methods

    @FormUrlEncoded
    @POST("api/MOM/fsearchprocessed")
    public Call<OrderDetails> GetCompletedOrderFiltersForFeildEngineer(@Field("Dealerid") String id);

    @FormUrlEncoded
    @POST("api/MOM/fprocessedfill")
    public Call<OrderDetails> GetCompletedOrderDetailsForNameForFeildEngineer(
            @Field("Dealerid") String Dealerid,
            @Field("FilterType") String FilterType,
            @Field("DistrName") String DistrName,
            @Field("Distributorid") String Distributorid);


    @FormUrlEncoded
    @POST("api/MOM/fprocessedfill")
    public Call<OrderDetails> GetCompletedOrderDetailsForOrderNumberForFeildEngineer(
            @Field("Dealerid") String Dealerid,
            @Field("FilterType") String FilterType,
            @Field("OrderNumber") String OrderNumber);


    @FormUrlEncoded
    @POST("api/MOM/fprocessedfill")
    public Call<OrderDetails> GetCompletedOrderDetailsForOrderDateForFeildEngineer(
            @Field("Dealerid") String Dealerid,
            @Field("FilterType") String FilterType,
            @Field("OrderDate") String OrderDate);


    @FormUrlEncoded
    @POST("api/MOM/fporderdetails1")
    public Call<PartsDetails> GetCompletedOrderDetailsForFeildEngineer(
            @Field("Dealerid") String id,
            @Field("OrderNumber") String orderNumber);


    // change password
    @FormUrlEncoded
    @POST("api/MOM/ChangePassword")
    public Call<Login> ChangePassword(
            @Field("id") String id,
            @Field("Password") String Password,
            @Field("NewPassword") String NewPassword);


    // forgot password
    @FormUrlEncoded
    @POST("api/MOM/ForgotPassword")
    public Call<CommenResult> ForgotPassword(
            @Field("id") String id,
            @Field("email") String email);


    // For Geniune check
    // For Geniune check
    @FormUrlEncoded
    @POST("api/MOM/ScanedCheck")
    Call<CheckUnique> check_unique(
            @Field("partid") String SerialNumber);

    @FormUrlEncoded
    @POST("api/MOM/GenuineCheckHis")
    Call<CheckGenuine> check_genuine(
            @Field("UniqueID") String UniqueID,
            @Field("SerialID") String SerialID,
            @Field("UserType") String UserType,
            @Field("userid") String id,
            @Field("pickid") String pickid,
            @Field("CustomerName") String CustomerName,
            @Field("TransactionAddress") String TransactionAddress,
            @Field("ScannedBy") String ScannedBy);

    @FormUrlEncoded
    @POST("api/MOM/LogHistory")
    Call<WabcoEmployee> wabcoemp(
            @Field("UniqueID") String UniqueID,
            @Field("SerialID") String SerialID
    );

    @FormUrlEncoded
    @POST("api/MOM/Registerupdate")
    Call<UpdateUser> UpdateUser(
            @Field("id") String id,
            @Field("name") String name,
            @Field("phone") String phone,
            @Field("OTP") String otp,
            @Field("usertype") String usertype);

    @FormUrlEncoded
    @POST("api/MOM/SaveOTP")
    Call<OrderResponse> SentOTP(
            @Field("id") String userID,
            @Field("OTP")String mOtp);



    @FormUrlEncoded
    @POST("api/MOM/ASClogin")
    Call<ASCDetails> asccodecheck(
            @Field("ASC_Code") String asccode);



    @FormUrlEncoded
    @POST("api/MOM/autoPartsearch")
    public Call<SerialList> GetautoPartsearch(
            @Field("partid") String partid);


    // Get Distributor

    @GET("api/MOM/getwhatsnew_notification")
    public Call<WhatsNewView> whatsnew();
}
