package adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.ArrayList;

import addcart.CartDAO;
import addcart.CartDTO;
import models.dealer.wishlist.Wishlistdetails_model;


/**
 * Created by system01 on 5/22/2017.
 */

public class  View_wish_items_Adapter extends ArrayAdapter<String> {


    private Wishlistdetails_model wishlistdetails_model;
    private Context context;
    private ArrayList<String> ItemName, ItemDesc, Itempartnumber, Itempartcode, Itemparttype, ItemImage;
    private ArrayList<Integer> Itemid, Itemprice, ItemWishid;
    private View row;
    private CartDAO cartDAO;
    private boolean[] checkBoxState;

    public
    View_wish_items_Adapter(Context context, Wishlistdetails_model wishlistdetails_model, ArrayList < Integer > Itemid, ArrayList < String > ItemName, ArrayList < String > ItemDesc, ArrayList < String > Itempartnumber, ArrayList < String > Itempartcode, ArrayList < String > Itemparttype, ArrayList < String > ItemImage, ArrayList < Integer > Itemprice, ArrayList < Integer > ItemWishid)
    {
        super(context, R.layout.adapter_view_whishlist_items, wishlistdetails_model.getpartnameList()  );
        this.context = context;
        this.Itemid = Itemid;
        this.ItemName = ItemName;
        this.ItemDesc = ItemDesc;
        this.Itempartnumber = Itempartnumber;
        this.Itempartcode = Itempartcode;
        this.Itemparttype = Itemparttype;
        this.ItemImage = ItemImage;
        this.Itemprice = Itemprice;
        this.ItemWishid = ItemWishid;
        this.wishlistdetails_model = wishlistdetails_model;
        //int count = wishlistdetails_model.getpartidList().size();
        //checkBoxState = new boolean[count];
        this.checkBoxState = checkBoxState;
        cartDAO = new CartDAO(context);
    }

    @Override
    public int getCount () {
        return wishlistdetails_model.getpartidList().size();
    }

    @Override
    public View getView ( final int position, View convertView, ViewGroup parent){
        // TODO Auto-generated method stub
        row = convertView;
        WishlistitemHolder holder = null;

        if (row == null) {
            try {
                row = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).
                        inflate(R.layout.adapter_view_whishlist_items, parent, false);
                holder = new WishlistitemHolder(row);
                row.setTag(holder);
                checkBoxState = new boolean[wishlistdetails_model.getpartidList().size()];

            }catch (Exception ex)
            {
                Log.v("Error", ex.getMessage());
            }


        } else {
            holder = (WishlistitemHolder) row.getTag();
        }
        try {
            holder.Product_name.setText(wishlistdetails_model.getpartnameList().get(position));
            holder.Description.setText(wishlistdetails_model.getdescriptionList().get(position));
            holder.Product_no.setText(wishlistdetails_model.getpartnumberList().get(position));

            //holder.Select.setTag(new Integer(position));
            //VITAL PART!!! Set the state of the
            //CheckBox using the boolean array
            holder.Select.setChecked(checkBoxState[position]);

//for managing the state of the boolean
            //array according to the state of the
            //CheckBox
            Log.v("Error", String.valueOf(checkBoxState[position]));
            holder.Cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CartDTO cartDTO = new CartDTO();
                    cartDTO.setPartNoList(wishlistdetails_model.getpartnumberList());
                    cartDTO.setPartdescriptionList(wishlistdetails_model.getdescriptionList());
                    cartDTO.setPartImageList(wishlistdetails_model.getImageList());
                    cartDTO.setPartNameList(wishlistdetails_model.getpartnameList());
                    cartDAO.addToCart(cartDTO,position);

                }
            });
            holder.Select.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    if(((CheckBox)v).isChecked()) {
                        checkBoxState[position] = true;
                        Itemid.add(wishlistdetails_model.getpartidList().get(position));
                        ItemName.add(wishlistdetails_model.getpartnameList().get(position));
                        ItemDesc.add(wishlistdetails_model.getdescriptionList().get(position));
                        ItemWishid.add(wishlistdetails_model.getWishIdList().get(position));
                        Itempartnumber.add(wishlistdetails_model.getpartnumberList().get(position));
                        Itempartcode.add(wishlistdetails_model.getpartcodeList().get(position));
                        Itemparttype.add(wishlistdetails_model.getparttypeList().get(position));
                        Itemprice.add(wishlistdetails_model.getpriceList().get(position));
                        ItemImage.add(wishlistdetails_model.getImageList().get(position));
                    }
                    else {
                        checkBoxState[position] = false;
                        Itemid.remove(wishlistdetails_model.getpartidList().get(position));
                        ItemName.remove(wishlistdetails_model.getpartnameList().get(position));
                        ItemDesc.remove(wishlistdetails_model.getdescriptionList().get(position));
                        ItemWishid.remove(wishlistdetails_model.getWishIdList().get(position));
                        Itempartnumber.remove(wishlistdetails_model.getpartnumberList().get(position));
                        Itempartcode.remove(wishlistdetails_model.getpartcodeList().get(position));
                        Itemparttype.remove(wishlistdetails_model.getparttypeList().get(position));
                        Itemprice.remove(wishlistdetails_model.getpriceList().get(position));
                        ItemImage.remove(wishlistdetails_model.getImageList().get(position));
                    }

                }
            });
      /*      if (checkBoxState[position])
                holder.Select.setChecked(true);
            else
                holder.Select.setChecked(false);



            holder.Select.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Integer pos = (Integer) buttonView.getTag();

                    if (isChecked) {
                        checkBoxState[pos.intValue()] = true;
                    } else {
                        checkBoxState[pos.intValue()] = false;


                    }
                }
            });
*/




          /*  holder.Select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((CheckBox) v).isChecked()) {

                        Itemid.add(wishlistdetails_model.getpartidList().get(position));
                        ItemName.add(wishlistdetails_model.getpartnameList().get(position));
                        ItemDesc.add(wishlistdetails_model.getdescriptionList().get(position));
                        ItemWishid.add(wishlistdetails_model.getWishIdList().get(position));
                        Itempartnumber.add(wishlistdetails_model.getpartnumberList().get(position));
                        Itempartcode.add(wishlistdetails_model.getpartcodeList().get(position));
                        Itemparttype.add(wishlistdetails_model.getparttypeList().get(position));
                        Itemprice.add(wishlistdetails_model.getpriceList().get(position));
                        ItemImage.add(wishlistdetails_model.getImageList().get(position));

                    } else {
                        Itemid.remove(wishlistdetails_model.getpartidList().get(position));
                        ItemName.remove(wishlistdetails_model.getpartnameList().get(position));
                        ItemDesc.remove(wishlistdetails_model.getdescriptionList().get(position));
                        ItemWishid.remove(wishlistdetails_model.getWishIdList().get(position));
                        Itempartnumber.remove(wishlistdetails_model.getpartnumberList().get(position));
                        Itempartcode.remove(wishlistdetails_model.getpartcodeList().get(position));
                        Itemparttype.remove(wishlistdetails_model.getparttypeList().get(position));
                        Itemprice.remove(wishlistdetails_model.getpriceList().get(position));
                        ItemImage.remove(wishlistdetails_model.getImageList().get(position));
                    }

                }
            });*/
        } catch (Exception ex)
        {
            Log.v("Error", ex.getMessage());
        }
        return row;
    }

    public class WishlistitemHolder {
        TextView Product_name;
        TextView Description;
        CheckBox Select;
        Button Cart;
        TextView Product_no;

        public WishlistitemHolder(View v) {
            Product_name = (TextView) row.findViewById(R.id.product_name);
            Description = (TextView) row.findViewById(R.id.product_description);
            Select = (CheckBox) row.findViewById(R.id.select);
            Cart = (Button) row.findViewById(R.id.cart);
            Product_no = (TextView) row.findViewById(R.id.product_no);
        }
    }


}




        /*  CartDTO cartDTO = new CartDTO();
                    cartDTO.setPartNoList(vehicleDTO.getPartNoList());
                    cartDTO.setPartdescriptionList(vehicleDTO.getPartDespcriptionList());
                    cartDTO.setPartImageList(vehicleDTO.getPartImageList());
                    cartDTO.setPartNameList(vehicleDTO.getProductNameList());

                    cartDAO.addToCart(cartDTO,position);
*//*


                }
            });

            holder.Cart.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {

          *//*      CartDTO cartDTO = new CartDTO();
                    cartDTO.setPartNoList(vehicleDTO.getPartNoList());
                    cartDTO.setPartdescriptionList(vehicleDTO.getPartDespcriptionList());
                    cartDTO.setPartImageList(vehicleDTO.getPartImageList());
                    cartDTO.setPartNameList(vehicleDTO.getProductNameList());
                    cartDAO.addWishList(cartDTO,0);*//*


                }
            });

           // wishlistname.setText(Wishname.get(position));


          /*  Selectwish.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(((CheckBox)v).isChecked()){
                        //checkspos.add(position);
                        //checkspos.add(position,Wishid.get(position));
                        checksid.add(Wishid.get(position));
                        checkname.add(Wishname.get(position));
                    }
                    else
                    {
                        checksid.remove(Wishid.get(position));
                        checkname.remove(Wishname.get(position));
                    }*/

   /* @Override
    public View_wish_items_Adapter.CarttoHolderHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_view_whishlist_items, null);
        View_wish_items_Adapter.CarttoHolderHolder rcv = new View_wish_items_Adapter.CarttoHolderHolder(layoutView);
        return rcv;
    }
    @Override
    public void onBindViewHolder(View_wish_items_Adapter.CarttoHolderHolder holder, final int position)
    {

          holder.Product_name.setText(wishlistdetails_model.getpartnameList().get(position));
          holder.Description.setText(wishlistdetails_model.getdescriptionList().get(position));
          holder.Select.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(((CheckBox)v).isChecked()){
                    //checkspos.add(position);
                    //checkspos.add(position,Wishid.get(position));
                    Itemid.add(wishlistdetails_model.getpartidList().get(position));
                    ItemName.add(wishlistdetails_model.getpartnameList().get(position));
                    ItemDesc.add(wishlistdetails_model.getdescriptionList().get(position));
                    ItemWishid.add(wishlistdetails_model.getWishIdList().get(position));
                    Itempartnumber.add(wishlistdetails_model.getpartnumberList().get(position));
                    Itempartcode.add(wishlistdetails_model.getpartcodeList().get(position));
                    Itemparttype.add(wishlistdetails_model.getparttypeList().get(position));
                    Itemprice.add(wishlistdetails_model.getpriceList().get(position));
                    ItemImage.add(wishlistdetails_model.getImageList().get(position));

                }
                else
                {
                    Itemid.remove(wishlistdetails_model.getpartidList().get(position));
                    ItemName.remove(wishlistdetails_model.getpartnameList().get(position));
                    ItemDesc.remove(wishlistdetails_model.getdescriptionList().get(position));
                    ItemWishid.remove(wishlistdetails_model.getWishIdList().get(position));
                    Itempartnumber.remove(wishlistdetails_model.getpartnumberList().get(position));
                    Itempartcode.remove(wishlistdetails_model.getpartcodeList().get(position));
                    Itemparttype.remove(wishlistdetails_model.getparttypeList().get(position));
                    Itemprice.remove(wishlistdetails_model.getpriceList().get(position));
                    ItemImage.remove(wishlistdetails_model.getImageList().get(position));
                }
              *//*  CartDTO cartDTO = new CartDTO();
                cartDTO.setPartNoList(vehicleDTO.getPartNoList());
                cartDTO.setPartdescriptionList(vehicleDTO.getPartDespcriptionList());
                cartDTO.setPartImageList(vehicleDTO.getPartImageList());
                cartDTO.setPartNameList(vehicleDTO.getProductNameList());

                cartDAO.addToCart(cartDTO,position);
*//*


            }
        });

        holder.Cart.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

          *//*      CartDTO cartDTO = new CartDTO();
                cartDTO.setPartNoList(vehicleDTO.getPartNoList());
                cartDTO.setPartdescriptionList(vehicleDTO.getPartDespcriptionList());
                cartDTO.setPartImageList(vehicleDTO.getPartImageList());
                cartDTO.setPartNameList(vehicleDTO.getProductNameList());
                cartDAO.addWishList(cartDTO,0);*//*


            }
        });
    }



  *//*  @Override
    public int getItemCount()
    {
        return this.wishlistdetails_model.getpartidList().size();
    }*//*
    public static class CarttoHolderHolder extends RecyclerView.ViewHolder {


        TextView Product_name;
        TextView Description;
        CheckBox Select;
        Button Cart;




        public CarttoHolderHolder(View v)
        {
            super(v);

            Product_name =  (TextView) v.findViewById(R.id.product_name);
            Description =(TextView)v.findViewById(R.id.product_description);
            Select = (CheckBox) v.findViewById(R.id.select);
            Cart = (Button) v.findViewById(R.id.cart);
        }
    }
*/

