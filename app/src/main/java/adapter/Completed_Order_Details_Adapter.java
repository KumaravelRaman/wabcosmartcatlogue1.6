package adapter;

import static android.content.Context.MODE_PRIVATE;
import static com.wabco.brainmagic.wabco.catalogue.R.id.sno;

import java.util.List;

import com.wabco.brainmagic.wabco.catalogue.R;

import alertbox.Alertbox;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import api.APIService;
import api.ApiUtils;
import models.dealer.pending.request.query.QueryPart;
import models.dealer.pending.responce.CommenResult;
import models.dealer.pending.responce.OrderDetailsResult;
import models.dealer.pending.responce.PartsDetailsResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Completed_Order_Details_Adapter extends ArrayAdapter<PartsDetailsResult> {
    private final SharedPreferences dealershare;
    private final SharedPreferences.Editor dealeredit;
    private Context context;
    private List<PartsDetailsResult> partsDetailsResult;
    private String SelectedReason;
    private OrderDetailsResult orderDetailsResult;
    private QueryPart querypart;
    private Alertbox box;

    public Completed_Order_Details_Adapter(Context context, List<PartsDetailsResult> partsDetailsResult,
                                           OrderDetailsResult orderDetailsResult) {
        super(context, R.layout.adapter_completed_order_details, partsDetailsResult);
        this.partsDetailsResult = partsDetailsResult;
        this.context = context;
        this.orderDetailsResult = orderDetailsResult;
        dealershare = context.getSharedPreferences("registration", MODE_PRIVATE);
        dealeredit = dealershare.edit();
        box = new Alertbox(context);
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final PendingOrder pendingOrderHolder;
        if (convertView == null) {
            convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater"))
                    .inflate(R.layout.adapter_completed_order_details, null);
            pendingOrderHolder = new PendingOrder();
            pendingOrderHolder.sno = (TextView) convertView.findViewById(sno);
            pendingOrderHolder.partnumber = (TextView) convertView.findViewById(R.id.partnumber);
            pendingOrderHolder.partName = (TextView) convertView.findViewById(R.id.partName);
            pendingOrderHolder.orderMrp = (TextView) convertView.findViewById(R.id.ordermrp);
            pendingOrderHolder.PartDescription =
                    (TextView) convertView.findViewById(R.id.PartDescription);
            pendingOrderHolder.ordedqty = (TextView) convertView.findViewById(R.id.ordedqty);

            pendingOrderHolder.dispatchedqty = (TextView) convertView.findViewById(R.id.dispatchedqty);
            pendingOrderHolder.status = (TextView) convertView.findViewById(R.id.status);


            convertView.setTag(pendingOrderHolder);
        } else {
            pendingOrderHolder = (PendingOrder) convertView.getTag();
        }

        pendingOrderHolder.sno.setText(Integer.toString(position + 1) + ".");

        pendingOrderHolder.partnumber
                .setText(partsDetailsResult.get(position).getPartNumber().toString());
        pendingOrderHolder.partName.setText(partsDetailsResult.get(position).getPartName());
        pendingOrderHolder.orderMrp.setText(context.getString(R.string.Rs) + " "
                + partsDetailsResult.get(position).getPartPrice().toString() + "0");
        pendingOrderHolder.PartDescription
                .setText(partsDetailsResult.get(position).getDescription().toString());
        pendingOrderHolder.ordedqty.setText(partsDetailsResult.get(position).getQuantity());

        if (partsDetailsResult.get(position).getDispatchedQty() == null)
            pendingOrderHolder.dispatchedqty.setText("0");
        else
            pendingOrderHolder.dispatchedqty.setText(partsDetailsResult.get(position).getDispatchedQty());

        pendingOrderHolder.status.setText(partsDetailsResult.get(position).getDeliveryStatus());


        return convertView;

    }


    private void SendQueryOrderedPartItem() {

        final ProgressDialog loading =
                ProgressDialog.show(context, "Sending", "Please wait...", false, false);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();

        APIService api = retrofit.create(APIService.class);
        Call<CommenResult> filtersCall;

        filtersCall = api.SendQueries(querypart);
        Log.v("Qurey part data",querypart.getQuery());

        filtersCall.enqueue(new Callback<CommenResult>() {
            @Override
            public void onResponse(Call<CommenResult> call, Response<CommenResult> response) {

                loading.dismiss();

                if (response.isSuccessful()) {
                    box.showAlertbox( response.body().getData());
                } else {
                    loading.dismiss();
                    // handle request errors yourself
                    box.showAlertbox(context.getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<CommenResult> call, Throwable t) {
                loading.dismiss();
                t.printStackTrace();
                box.showAlertbox(context.getString(R.string.slow_internet_connection));

            }
        });

    }


    class PendingOrder {
        TextView partnumber, partName, PartDescription, orderMrp, ordedqty, dispatchedqty, status, sno;
    }


}
