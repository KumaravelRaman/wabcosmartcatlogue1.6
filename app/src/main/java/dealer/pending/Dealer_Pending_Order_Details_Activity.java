package dealer.pending;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

import adapter.Pending_Order_Details_Adapter;
import addcart.CartDAO;
import addcart.CartDTO;
import alertbox.Alertbox;
import api.APIService;
import api.ApiUtils;
import askwabco.AskWabcoActivity;
import dealer.account.Dealer_Account_Activity;
import directory.WabcoUpdate;
import home.MainActivity;
import models.dealer.pending.request.cancel.Cancel;
import models.dealer.pending.request.cancel.CancelOrder;
import models.dealer.pending.request.cancel.CancelPart;
import models.dealer.pending.responce.CancelSingleOrderResult;
import models.dealer.pending.responce.OrderDetailsResult;
import models.dealer.pending.responce.PartsDetails;
import models.dealer.pending.responce.PartsDetailsResult;
import network.NetworkConnection;
import notification.NotificationActivity;
import okhttp3.OkHttpClient;
import pekit.PE_Kit_Activity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import quickorder.Quick_Order_Preview_Activity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import search.SearchActivity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;

public class Dealer_Pending_Order_Details_Activity extends Activity {

  private ImageView Backbtn,Cart_Icon;
  private View heade_Layout;
  private TextView Tittle;
  private Button Cancelbtn;
  private OrderDetailsResult orderDetailsResult;
  private ImageView Accountbtn;
  private TextView OrderNumberTxt, DistributorTxt, TotalMRPTxt, OrderDateTxt;
  private ListView listView;
  private Alertbox box = new Alertbox(Dealer_Pending_Order_Details_Activity.this);
  private SharedPreferences dealershare;
  private SharedPreferences.Editor dealeredit;
  private Button Updatebtn;
  private MaterialSpinner Partnumber_filter, Status_filter;
  private String SelectedPartNumber, SelectedPartStatus, SelectedReason = "";
  private Pending_Order_Details_Adapter adapter;
  private List<PartsDetailsResult> partsDetailsResults, partsDetails;
  private CancelSingleOrderResult cancelResultData;
  private CancelOrder cancelorder;
  private AlertDialog alertDialogAndroid;
  private ImageView Menubtn;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_dealer_pending_order__details);

    dealershare = getSharedPreferences("registration", MODE_PRIVATE);
    dealeredit = dealershare.edit();

    heade_Layout = findViewById(R.id.header_layout);
    Tittle = (TextView) heade_Layout.findViewById(R.id.tittle);
    Tittle.setText("Pending Orders Details");


    Accountbtn = (ImageView) heade_Layout.findViewById(R.id.account_icon);
    Backbtn = (ImageView) heade_Layout.findViewById(R.id.back);
    Cart_Icon = (ImageView) heade_Layout.findViewById(R.id.cart_icon);

    Partnumber_filter = (MaterialSpinner) findViewById(R.id.partnumber_filter);
    Status_filter = (MaterialSpinner) findViewById(R.id.status_filter);

    Partnumber_filter.setBackgroundResource(R.drawable.autotextback);
    Status_filter.setBackgroundResource(R.drawable.autotextback);


    Updatebtn = (Button) findViewById(R.id.update);
    listView = (ListView) findViewById(R.id.listView);

    OrderNumberTxt = (TextView) findViewById(R.id.order_number);
    DistributorTxt = (TextView) findViewById(R.id.distributor_name);
    TotalMRPTxt = (TextView) findViewById(R.id.totalMrp);
    OrderDateTxt = (TextView) findViewById(R.id.orderdate);


    orderDetailsResult = (OrderDetailsResult) getIntent().getSerializableExtra("orderObject");
    DistributorTxt.setText(orderDetailsResult.getDistrName());
    OrderNumberTxt.setText(orderDetailsResult.getOrderNumber());
    TotalMRPTxt.setText(getResources().getString(R.string.Rs) + " "
        + orderDetailsResult.getTotalOrderAmount() + "0");


    String[] splitdate = orderDetailsResult.getOrderedDate().split("T");
    OrderDateTxt.setText(splitdate[0]);

    Partnumber_filter.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
      @Override
      public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

        SelectedPartNumber = item.toString();

        if (!SelectedPartNumber.equals("All")) {
          List<PartsDetailsResult> resultList = new ArrayList<PartsDetailsResult>();
          for (PartsDetailsResult P : partsDetails) {
            if (P.getPartNumber().equals(SelectedPartNumber)) {
              resultList.add(P);
              adapter.notifyDataSetChanged();
              listView.setAdapter(new Pending_Order_Details_Adapter(
                  Dealer_Pending_Order_Details_Activity.this, resultList, orderDetailsResult));
            }
          }
          Status_filter.setSelectedIndex(1);
        } else {
          adapter = new Pending_Order_Details_Adapter(Dealer_Pending_Order_Details_Activity.this,
              partsDetailsResults, orderDetailsResult);
          listView.setAdapter(adapter);
        }
      }
    });

    Status_filter.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
      @Override
      public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

        SelectedPartStatus = item.toString();
        if (!SelectedPartStatus.equals("All")) {
          List<PartsDetailsResult> resultList = new ArrayList<PartsDetailsResult>();
          for (PartsDetailsResult P : partsDetails) {

            if (P.getDeliveryStatus().equals(SelectedPartStatus)) {
              resultList.add(P);
              adapter.notifyDataSetChanged();
              listView.setAdapter(new Pending_Order_Details_Adapter(
                  Dealer_Pending_Order_Details_Activity.this, resultList, orderDetailsResult));
            }
          }
          Partnumber_filter.setSelectedIndex(1);
        } else {
          adapter = new Pending_Order_Details_Adapter(Dealer_Pending_Order_Details_Activity.this,
              partsDetailsResults, orderDetailsResult);
          listView.setAdapter(adapter);
        }

      }
    });


    Accountbtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(
            new Intent(Dealer_Pending_Order_Details_Activity.this, Dealer_Account_Activity.class));
      }
    });



    Cart_Icon.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        CartDAO cartDAO = new CartDAO(getApplicationContext());
        CartDTO cartDTO = cartDAO.GetCartItems();
        if(cartDTO.getPartCodeList() == null)
        {
          StyleableToast st =
                  new StyleableToast(getApplicationContext(), "Cart is Empty !", Toast.LENGTH_SHORT);
          st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();
        }else
        {
          startActivity(new Intent(getApplicationContext(), Quick_Order_Preview_Activity.class).putExtra("from","CartItem"));
        }
      }
    });

    Backbtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(
                new Intent(getApplicationContext(), Dealer_Pending_Order_Activity.class));
      }
    });


    Updatebtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // GetCancelOrederedData();

        boolean cancelled = false;

        for (int i = 0; i < adapter.getCount(); i++) {
          if (partsDetailsResults.get(i).isCancelled()) {
            cancelled = true;
          }
        }
        if (cancelled) {
          ShowReasonDialog();
        } else {
          StyleableToast st = new StyleableToast(Dealer_Pending_Order_Details_Activity.this,
                  "Please make any changes in the order for updation !", Toast.LENGTH_SHORT);
          st.setBackgroundColor(
              Dealer_Pending_Order_Details_Activity.this.getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();
        }

      }
    });




    Menubtn = (ImageView) heade_Layout.findViewById(R.id.menu);
    Menubtn.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(Dealer_Pending_Order_Details_Activity.this, R.style.PopupMenu);
        final PopupMenu pop = new PopupMenu(wrapper, v);
        pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

          public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
              case R.id.home:
                startActivity(new Intent(Dealer_Pending_Order_Details_Activity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
              case R.id.search:
                startActivity(new Intent(Dealer_Pending_Order_Details_Activity.this, SearchActivity.class));
                break;
              case R.id.notification:
                startActivity(new Intent(Dealer_Pending_Order_Details_Activity.this, NotificationActivity.class));
                break;
              case R.id.vehicle:
                startActivity(new Intent(Dealer_Pending_Order_Details_Activity.this, VehicleMakeActivity.class));
                break;
              case R.id.product:
                startActivity(new Intent(Dealer_Pending_Order_Details_Activity.this, ProductFamilyActivity.class));
                break;
              case R.id.performance:
                startActivity(new Intent(Dealer_Pending_Order_Details_Activity.this, PE_Kit_Activity.class));
                break;
              case R.id.contact:
                startActivity(new Intent(Dealer_Pending_Order_Details_Activity.this, Network_Activity.class));
                break;
              case R.id.askwabco:
                startActivity(new Intent(Dealer_Pending_Order_Details_Activity.this, AskWabcoActivity.class));
                break;
              case R.id.pricelist:
                startActivity(new Intent(Dealer_Pending_Order_Details_Activity.this, PriceListActivity.class));
                break;
              case R.id.update:
                WabcoUpdate update = new WabcoUpdate(Dealer_Pending_Order_Details_Activity.this);
                update.checkVersion();
                break;
            }
            return false;
          }
        });
        pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

          @Override
          public void onDismiss(PopupMenu arg0) {
            // TODO Auto-generated method stub
            pop.dismiss();
          }
        });

        pop.inflate(R.menu.main);
        pop.show();
      }
    });


    CheckInternet();
  }


  private void GetCancelOrederedData() {
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    Date date = new Date();
    System.out.println(dateFormat.format(date));
    cancelorder = new CancelOrder();
    List<CancelPart> cancelPartList = new ArrayList<CancelPart>();
    Cancel cancel = new Cancel();

    for (int i = 0; i < adapter.getCount(); i++) {
      if (partsDetailsResults.get(i).isCancelled()) {

        CancelPart cancelPart = new CancelPart();
        cancelPart.setPartName(partsDetailsResults.get(i).getPartName());
        cancelPart.setDescription(partsDetailsResults.get(i).getDescription());
        cancelPart.setPartNumber(partsDetailsResults.get(i).getPartNumber());
        cancelPart.setPartPrice(partsDetailsResults.get(i).getPartPrice());
        cancelPart.setQuantity(partsDetailsResults.get(i).getQuantity());
        cancelPart.setReason(SelectedReason);
        cancelPart.setCancelledBy(partsDetailsResults.get(i).getCancelledBy());
        cancelPart.setOrderNumber(partsDetailsResults.get(i).getOrderNumber());
        cancelPart.setUpdatedDate(dateFormat.format(date));
        cancelPart.setActive("N");
        cancelPartList.add(cancelPart);

        cancel.setDealerid(dealershare.getString("id", ""));
        cancel.setDistributorid(partsDetailsResults.get(i).getDistributorid());
        cancel.setOrderNumber(partsDetailsResults.get(i).getOrderNumber());
        cancel.setOrderStatus("Cancelled");
        cancel.setActive("N");

      }

    }


  /*  if (areAllTrue(partsDetailsResults)) {
      cancel.setOrderStatus("Cancelled");
      cancel.setActive("N");
    }
    else {
      cancel.setOrderStatus("NotCancelled");
    }*/

    cancelorder.setCancelParts(cancelPartList);
    cancelorder.setCancel(cancel);


    if (new NetworkConnection(Dealer_Pending_Order_Details_Activity.this).CheckInternet()) {
      SendCancelOrderedPartItem(cancelorder);
    } else {
      box.showAlertbox(getResources().getString(R.string.nointernetmsg));
    }


  }

  private void ShowReasonDialog() {

    LayoutInflater layoutInflaterAndroid =
        LayoutInflater.from(Dealer_Pending_Order_Details_Activity.this);
    View mView = layoutInflaterAndroid.inflate(R.layout.alert_dialog_for_cancel_reason, null);
    AlertDialog.Builder alertDialogBuilderUserInput =
        new AlertDialog.Builder(Dealer_Pending_Order_Details_Activity.this);
    alertDialogBuilderUserInput.setView(mView);
    alertDialogBuilderUserInput.setCancelable(false).setTitle("WABCO");
    final MaterialSpinner reasonSpinner = (MaterialSpinner) mView.findViewById(R.id.reason_spinner);
     TextView reasonTittle = (TextView) mView.findViewById(R.id.dialogTitle);
    reasonTittle.setText("Canceling Part Items");
    reasonSpinner.setBackgroundResource(R.drawable.autotextback);
    reasonSpinner.setItems(getResources().getStringArray(R.array.reasons));
    reasonSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
      @Override
      public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
        SelectedReason = item.toString();
      }
    });

    alertDialogBuilderUserInput.setPositiveButton("Save", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialogBox, int id) {
        // ToDo get user input here

        if (!reasonSpinner.getText().toString().equals("Select your reason")) {
          SelectedReason = reasonSpinner.getText().toString();
          GetCancelOrederedData();
        } else {
          StyleableToast st = new StyleableToast(Dealer_Pending_Order_Details_Activity.this,
              "Select your reason !", Toast.LENGTH_SHORT);
          st.setBackgroundColor(getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();
        }
      }
    });

    alertDialogBuilderUserInput.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialogBox, int id) {

        dialogBox.cancel();
      }
    });

    alertDialogAndroid = alertDialogBuilderUserInput.create();
    alertDialogAndroid.show();
    //alertDialogAndroid.getWindow().setLayout(600, 400);

  }

  public static boolean areAllTrue(List<PartsDetailsResult> array) {
    for (PartsDetailsResult b : array)
      if (!b.isCancelled())
        return false;
    return true;
  }

  public static boolean checkStatus(List<PartsDetailsResult> array) {
    int i = 0;
    for (PartsDetailsResult b : array)
      if (b.getDeliveryStatus().equals("Pending")) {
        i = i + 1;
      }
    if (i == 1)
      if(areAllTrue(array))
      return true;
      else return false;
    else
      return false;
  }

  private void CheckInternet() {

    if (new NetworkConnection(Dealer_Pending_Order_Details_Activity.this).CheckInternet()) {
      GetOrderedData();
    } else {
      box.showAlertbox(getResources().getString(R.string.nointernetmsg));
    }
  }

  private void GetOrderedData() {
    final ProgressDialog loading = ProgressDialog.show(Dealer_Pending_Order_Details_Activity.this,
        "Loading", "Please wait", false, false);

    Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create()).build();
    APIService api = retrofit.create(APIService.class);
    Call<PartsDetails> filtersCall = api.GetPendingOrderDetails(dealershare.getString("id", ""),
        orderDetailsResult.getOrderNumber());
    filtersCall.enqueue(new Callback<PartsDetails>() {

      @Override
      public void onResponse(Call<PartsDetails> call, Response<PartsDetails> response) {

        loading.dismiss();

        if (response.isSuccessful()) {
          response.body().getResult().toString();
          if(response.body().getResult().equals("Not Found"))
          {
            box.showNegativebox("This order has been processed!");
            //startActivity(new Intent(Dealer_Pending_Order_Details_Activity.this,Dealer_Pending_Order_Activity.class).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));
          }
          else
          {
            partsDetailsResults = response.body().getData();
            partsDetails = response.body().getData();
            LoadListviewData(response.body().getData());
          }

        } else {
          loading.dismiss(); // handle request errors yourself
          box.showAlertbox(getResources().getString(R.string.server_error));
        }
      }

      @Override
      public void onFailure(Call<PartsDetails> call, Throwable t) {
        loading.dismiss();
        t.printStackTrace();
        box.showAlertbox(getResources().getString(R.string.server_error));

      }
    });
  }

  private void LoadListviewData(List<PartsDetailsResult> result) {

    adapter = new Pending_Order_Details_Adapter(Dealer_Pending_Order_Details_Activity.this, result,
        orderDetailsResult);

    listView.setAdapter(adapter);
    List<String> partnumber = new ArrayList();
    List<String> partstatus = new ArrayList();
    partnumber.add("All");
    partstatus.add("All");
    partnumber.add("Select Part");
    partstatus.add("Select Status");
    for (int i = 0; i < result.size(); i++) {

      partnumber.add(partsDetailsResults.get(i).getPartNumber());
      Partnumber_filter.setItems(partnumber);

      partstatus.add(partsDetailsResults.get(i).getDeliveryStatus());
      partstatus = new ArrayList<String>(new LinkedHashSet<String>(partstatus));
      Status_filter.setItems(partstatus);

    }
  }


  private void SendCancelOrderedPartItem(CancelOrder cancelorder) {

    final ProgressDialog loading = ProgressDialog.show(Dealer_Pending_Order_Details_Activity.this,
        "Cancelling", "Please wait", false, false);

    OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(100, TimeUnit.SECONDS)
            .readTimeout(100,TimeUnit.SECONDS).build();
    Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL).client(client)
        .addConverterFactory(GsonConverterFactory.create()).build();

    APIService api = retrofit.create(APIService.class);
    Call<CancelSingleOrderResult> filtersCall;

    filtersCall = api.SendCancelPart(cancelorder);

    filtersCall.enqueue(new Callback<CancelSingleOrderResult>() {
      @Override
      public void onResponse(Call<CancelSingleOrderResult> call,
          Response<CancelSingleOrderResult> response) {

        loading.dismiss();

        if (response.isSuccessful()) {
          cancelResultData = response.body();

          if (cancelResultData.getResult().equals("Success")) {
            OrderCancellSuccess(response.body().getData());
          } else {
            OrderCancellFailed(response.body().getData());
          }


        } else {
          loading.dismiss();
          // handle request errors yourself
          box.showAlertbox(getResources().getString(R.string.server_error));
        }
      }

      @Override
      public void onFailure(Call<CancelSingleOrderResult> call, Throwable t) {
        loading.dismiss();
        t.printStackTrace();
        box.showAlertbox(getResources().getString(R.string.server_error));

      }
    });



  }

  private void OrderCancellFailed(String ordernumber) {
    final AlertDialog.Builder alertDialog =
        new AlertDialog.Builder(Dealer_Pending_Order_Details_Activity.this);
    alertDialog.setMessage("Something went wrong. Selected part number(s) status did not get updated!");
    alertDialog.setTitle("WABCO");
    alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        recreate();
      }
    });
    // alertDialog.setIcon(R.drawable.logo);
    alertDialog.show();
  }

  private void OrderCancellSuccess(String ordernumber) {

    final AlertDialog.Builder alertDialog =
        new AlertDialog.Builder(Dealer_Pending_Order_Details_Activity.this);
    alertDialog.setMessage("Selected part number(s) were cancelled from this order!");
    alertDialog.setTitle("WABCO");
    alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {

        recreate();
      }
    });
    // alertDialog.setIcon(R.drawable.logo);
    alertDialog.show();

  }


}
