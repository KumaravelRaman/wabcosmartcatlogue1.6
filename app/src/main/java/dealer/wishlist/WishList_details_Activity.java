package dealer.wishlist;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.ArrayList;

import adapter.View_wish_items_Adapter;
import addcart.CartDAO;
import addcart.CartDTO;
import askwabco.AskWabcoActivity;
import dealer.account.Dealer_Account_Activity;
import directory.WabcoUpdate;
import home.MainActivity;
import models.dealer.wishlist.Wishlistdetails_model;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import persistence.DBHelper;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import quickorder.Quick_Order_Preview_Activity;
import search.SearchActivity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;

public class WishList_details_Activity extends Activity {
    private View heade_Layout;
    private ImageView Backbtn, Accountbtn,Cart_Icon,Menu;
    private TextView Tittle;
    String WishlistName;
    Integer WishID;
    private ProgressDialog progressDialog;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private ArrayList<String> partnameList,partnumberList,parttypeList,descriptionList,ImageList,partcodeList;
    private ArrayList<Integer> partidList,WishIdList,priceList;
    private ArrayList<String> ItemName,ItemDesc,Itempartnumber,Itempartcode,Itemparttype,ItemImage;
    private ArrayList<Integer> Itemid,Itemprice,ItemWishid;
    ListView listview;
    RecyclerView recycleview;
    TextView Wishlist_name;
    Wishlistdetails_model wishlistdetails = new Wishlistdetails_model();
    Button Remove;
    View_wish_items_Adapter Adapter;
    private ImageView Menubtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list_details);

        heade_Layout =  findViewById(R.id.header_layout);
        Tittle = (TextView) heade_Layout.findViewById(R.id.tittle);
        Backbtn = (ImageView) heade_Layout.findViewById(R.id.back);
        Accountbtn = (ImageView) heade_Layout.findViewById(R.id.account_icon);
        Cart_Icon = (ImageView) heade_Layout.findViewById(R.id.cart_icon);
        Menu = (ImageView) heade_Layout.findViewById(R.id.menu);

        partidList= new ArrayList<Integer>();
        WishIdList = new ArrayList<Integer>();
        partnameList = new ArrayList<String>();
        partnumberList = new ArrayList<String>();
        parttypeList = new ArrayList<String>();
        priceList = new ArrayList<Integer>();
        descriptionList = new ArrayList<String>();
        partcodeList = new ArrayList<String>();
        ImageList = new ArrayList<String>();

        Itemid = new ArrayList<Integer>();
        Itemprice = new ArrayList<Integer>();
        ItemWishid = new ArrayList<Integer>();
        ItemName = new ArrayList<String>();
        ItemDesc = new ArrayList<String>();
        Itempartnumber = new ArrayList<String>();
        Itempartcode = new ArrayList<String>();
        Itemparttype = new ArrayList<String>();
        ItemImage = new ArrayList<String>();

        listview  =  (ListView) findViewById(R.id.whishitem_list);
        //listview = (ListView)findViewById(R.id.whishlistitems);
        recycleview = (RecyclerView)findViewById(R.id.listitem);
        Wishlist_name = (TextView) findViewById(R.id.wishlist_name);
        Remove = (Button) findViewById(R.id.remove);
        progressDialog = new ProgressDialog(this);
        WishID = getIntent().getIntExtra ("ID",0);
        WishlistName = getIntent().getStringExtra("Listname");

        Wishlist_name.setText(String.format("Wish List Name : %s", WishlistName));
        new GetWishlist().execute();
        Adapter = new View_wish_items_Adapter(WishList_details_Activity.this, wishlistdetails,Itemid,ItemName,ItemDesc,Itempartnumber,Itempartcode,Itemparttype,ItemImage,Itemprice,ItemWishid);
        Accountbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(
                        new Intent(getApplicationContext(), Dealer_Account_Activity.class));
            }
        });

        Backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Cart_Icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                CartDAO cartDAO = new CartDAO(getApplicationContext());
                CartDTO cartDTO = cartDAO.GetCartItems();
                if(cartDTO.getPartCodeList() == null)
                {
                    StyleableToast st =
                            new StyleableToast(getApplicationContext(), "Cart is Empty !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else
                {
                    startActivity(new Intent(getApplicationContext(), Quick_Order_Preview_Activity.class).putExtra("from","CartItem"));
                }
                //  startActivity(new Intent(ProductFamilyActivity.this, Cart_Activity.class));
            }
        });
        Remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Itemid.size()==0)
                {
                    StyleableToast st = new StyleableToast(getApplicationContext(), "Select item to delete", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else {
                    //delete(Itemid);
                    for (int i = 0; i < Itemid.size(); i++) {
                        delete(Itemid);
                        partnameList.remove(ItemName.get(i));
                        descriptionList.remove(ItemDesc.get(i));
                        partidList.remove(Itemid.get(i));
                        WishIdList.remove(ItemWishid.get(i));
                        partnumberList.remove(Itempartnumber.get(i));
                        parttypeList.remove(Itemparttype.get(i));
                        priceList.remove(Itemprice.get(i));
                        partcodeList.remove(Itempartcode.get(i));
                        ImageList.remove(ItemImage.get(i));

                    }
                    wishlistdetails.setpartidList(partidList);
                    wishlistdetails.setWishIdList(WishIdList);
                    wishlistdetails.setpartnameList(partnameList);
                    wishlistdetails.setpartnumberList(partnumberList);
                    wishlistdetails.setpartcodeList(partcodeList);
                    wishlistdetails.setparttypeList(parttypeList);
                    wishlistdetails.setpriceList(priceList);
                    wishlistdetails.setdescriptionList(descriptionList);
                    wishlistdetails.setImageList(ImageList);
//                    Adapter.notifyDataSetChanged();

                    Itemid.clear();
                    ItemDesc.clear();
                    ItemName.clear();
                    ItemWishid.clear();
                    Itempartnumber.clear();
                    Itemparttype.clear();
                    ItemImage.clear();
                    Itemprice.clear();
                    Itempartcode.clear();
                    listview.setAdapter(Adapter);
                }

            }
        });




        Menubtn = (ImageView) heade_Layout.findViewById(R.id.menu);
        Menubtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(WishList_details_Activity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(WishList_details_Activity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(WishList_details_Activity.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(WishList_details_Activity.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(WishList_details_Activity.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(WishList_details_Activity.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(WishList_details_Activity.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(WishList_details_Activity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(WishList_details_Activity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(WishList_details_Activity.this, PriceListActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(WishList_details_Activity.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });




    }
    private class GetWishlist extends AsyncTask<String, Void, String> {

        Cursor cursor;
       // Wishlistdetails_model wishlistdetails = new Wishlistdetails_model();
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            dbHelper = new DBHelper(WishList_details_Activity.this);
            db = dbHelper.readDataBase();
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            Log.v("OFF Line", "SQLITE Table");
            try {
                String query = "select * from WishItems where WishId = " + WishID + "";
                Log.v("Dealer Name", query);
                cursor = db.rawQuery(query, null);
                if (cursor.moveToFirst()) {
                    do {
                        partidList.add(cursor.getInt(cursor.getColumnIndex("partid")));
                        WishIdList.add(cursor.getInt(cursor.getColumnIndex("WishId")));
                        partnameList.add(cursor.getString(cursor.getColumnIndex("partname")));
                        partnumberList.add(cursor.getString(cursor.getColumnIndex("partnumber")));
                        partcodeList.add(cursor.getString(cursor.getColumnIndex("partcode")));
                        parttypeList.add(cursor.getString(cursor.getColumnIndex("parttype")));
                        priceList.add(cursor.getInt(cursor.getColumnIndex("price")));
                        descriptionList.add(cursor.getString(cursor.getColumnIndex("description")));
                        ImageList.add(cursor.getString(cursor.getColumnIndex("image")));
                    } while (cursor.moveToNext());
                    cursor.close();
                    db.close();
                    retrieveALL();
                    return "received";
                } else {
                    cursor.close();
                    db.close();
                    return "nodata";
                }
            } catch (Exception e) {
                cursor.close();
                db.close();
                Log.v("Error in Incentive", e.getMessage());
                return "notsuccess";
            }
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            switch (result) {
                case "received":

                    listview.setAdapter(Adapter);
                    //LinearLayoutManager llm = new LinearLayoutManager(WishList_details_Activity.this);

                  //  llm.setOrientation(LinearLayoutManager.VERTICAL);
                    //recycleview.setLayoutManager(llm);
                    //recycleview.setAdapter(Adapter);
                    break;
                case "nodata":

                    final AlertDialog.Builder alertDialog =
                            new AlertDialog.Builder(WishList_details_Activity.this);
                    alertDialog.setTitle("WABCO");
                    alertDialog.setMessage("Wishlist is empty !");
                    alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    });

                    alertDialog.show();

                    break;
                default:

                    break;
            }

        }

    }

    public Wishlistdetails_model retrieveALL() {

        wishlistdetails.setpartidList(partidList);
        wishlistdetails.setWishIdList(WishIdList);
        wishlistdetails.setpartnameList(partnameList);
        wishlistdetails.setpartnumberList(partnumberList);
        wishlistdetails.setpartcodeList(partcodeList);
        wishlistdetails.setparttypeList(parttypeList);
        wishlistdetails.setpriceList(priceList);
        wishlistdetails.setdescriptionList(descriptionList);
        wishlistdetails.setImageList(ImageList);
        return wishlistdetails;
    }

    private void delete(ArrayList<Integer> IDlist) {
        try {
            SQLiteDatabase db = null;
            DBHelper dbhelper = new DBHelper(WishList_details_Activity.this);
            db = dbhelper.readDataBase();
            for (int i = 0; i < IDlist.size(); i++) {

                db.execSQL("delete from WishItems where partid = '" + IDlist.get(i) + "'");
            }
            db.close();
        } catch (Exception ex) {
            Log.v("Xcept", ex.getMessage());
        }
    }
}
