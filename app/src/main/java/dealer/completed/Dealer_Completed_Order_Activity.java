package dealer.completed;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import adapter.Completed_Order_Adapter;
import addcart.CartDAO;
import addcart.CartDTO;
import alertbox.Alertbox;
import api.APIService;
import api.ApiUtils;
import askwabco.AskWabcoActivity;
import dealer.account.Dealer_Account_Activity;
import directory.WabcoUpdate;
import home.MainActivity;
import models.dealer.pending.responce.OrderDetails;
import models.dealer.pending.responce.OrderDetailsResult;
import network.NetworkConnection;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import quickorder.Quick_Order_Preview_Activity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import search.SearchActivity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;

public class Dealer_Completed_Order_Activity extends AppCompatActivity {

    private TextView Order_number;
    private ImageView Backbtn,Accountbtn;
    private View heade_Layout;
    private TextView Tittle;
    private SharedPreferences dealershare;
    private SharedPreferences.Editor dealeredit;
    private MaterialSpinner filterType, filter;
    private Alertbox box = new Alertbox(Dealer_Completed_Order_Activity.this);
    private ListView listView;
    private List<String> DistributorID;
    private String SelectedDistributorID;
    private String SelecetedFiterText;
    private String SelectedFilter;
    private String DealerID;
    private Completed_Order_Adapter adapter;
    private List<OrderDetailsResult> Order_result;
    private ImageView back;
    private ImageView Menubtn;
    private ImageView Cart_Icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer__complete_order);

        dealershare = getSharedPreferences("registration", MODE_PRIVATE);
        dealeredit = dealershare.edit();

        heade_Layout =  findViewById(R.id.header_layout);
        Tittle = (TextView) heade_Layout.findViewById(R.id.tittle);
        Backbtn = (ImageView) heade_Layout.findViewById(R.id.back);
        listView = (ListView) findViewById(R.id.listView);
        Accountbtn = (ImageView) heade_Layout.findViewById(R.id.account_icon);
        Cart_Icon = (ImageView) heade_Layout.findViewById(R.id.cart_icon);
        filterType = (MaterialSpinner) findViewById(R.id.filterType);
        filter = (MaterialSpinner) findViewById(R.id.filter);
        String[] filterTpes = {"Select filter type", "Distributor Name", "Order Date", "Order Number"};
        filterType.setItems(filterTpes);

        Tittle.setText("Completed Orders");


        Backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        Cart_Icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CartDAO cartDAO = new CartDAO(getApplicationContext());
                CartDTO cartDTO = cartDAO.GetCartItems();
                if(cartDTO.getPartCodeList() == null)
                {
                    StyleableToast st =
                            new StyleableToast(getApplicationContext(), "Cart is Empty !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else
                {
                    startActivity(new Intent(getApplicationContext(), Quick_Order_Preview_Activity.class).putExtra("from","CartItem"));
                }
            }
        });

        filterType.setBackgroundResource(R.drawable.autotextback);
        filter.setBackgroundResource(R.drawable.autotextback);

        DistributorID = new ArrayList<String>();
        DealerID = dealershare.getString("id", "").toString();

        Accountbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(
                        new Intent(Dealer_Completed_Order_Activity.this, Dealer_Account_Activity.class));
            }
        });

        Backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        filterType.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                if (!item.toString().equals("Select filter type"))
                    CheckInternet();
                else {
                    LoadListviewData();
                    LoadFilters(Order_result);
                }
            }
        });
        filter.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                if (item.toString().equals("Select filter")) {
                    StyleableToast st = new StyleableToast(Dealer_Completed_Order_Activity.this,
                            "Select filter !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                } else {
                    if (SelecetedFiterText.equals("Distributor Name")) {
                        SelectedDistributorID = DistributorID.get(position - 1).toString();
                    }
                    SelectedFilter = item.toString();
                    GetCompletedOrderDetailsForFilter();

                }


            }
        });


        Menubtn = (ImageView) heade_Layout.findViewById(R.id.menu);
        Menubtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(Dealer_Completed_Order_Activity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(Dealer_Completed_Order_Activity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(Dealer_Completed_Order_Activity.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(Dealer_Completed_Order_Activity.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(Dealer_Completed_Order_Activity.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(Dealer_Completed_Order_Activity.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(Dealer_Completed_Order_Activity.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(Dealer_Completed_Order_Activity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(Dealer_Completed_Order_Activity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(Dealer_Completed_Order_Activity.this, PriceListActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(Dealer_Completed_Order_Activity.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });



        CheckInternet();

    }

    @Override
    protected void onResume() {
        super.onResume();
        CheckInternet();
    }

    private void CheckInternet() {

        if (new NetworkConnection(Dealer_Completed_Order_Activity.this).CheckInternet()) {
            GetFilteredData();
        } else {
            box.showAlertbox(getResources().getString(R.string.nointernetmsg));
        }
    }


    // This for Get Spinner data
    private void GetFilteredData() {

        final ProgressDialog loading = ProgressDialog.show(Dealer_Completed_Order_Activity.this,
                "Loading", "Please wait", false, false);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();
        APIService api = retrofit.create(APIService.class);
        Call<OrderDetails> filtersCall = api.GetCompletedOrderFilters(dealershare.getString("id", ""));
        filtersCall.enqueue(new Callback<OrderDetails>() {
            @Override
            public void onResponse(Call<OrderDetails> call, Response<OrderDetails> response) {

                loading.dismiss();

                if (response.isSuccessful()) {
                    Order_result = response.body().getData();
                    LoadFilters(Order_result);
                    LoadListviewData();

                } else {
                    loading.dismiss();
                    // handle request errors yourself
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<OrderDetails> call, Throwable t) {
                loading.dismiss();
                t.printStackTrace();
                box.showAlertbox(getResources().getString(R.string.server_error));

            }
        });


    }
    // Load data for spinner
    private void LoadFilters(List<OrderDetailsResult> filters) {

        SelecetedFiterText = filterType.getText().toString();

        if (filters.size() == 0) {
            box.showNegativebox("No orders found !");
        } else {
            List<String> distributordata = new ArrayList();
            distributordata.add(0, "Select filter");
            Set<String> statushs = new HashSet<>();
            DistributorID.clear();
            if (SelecetedFiterText.equals("Distributor Name")) {
                for (int i = 0; i < filters.size(); i++) {
                    distributordata.add(filters.get(i).getDistrName());
                    distributordata = new ArrayList<String>(new LinkedHashSet<String>(distributordata));
                    filter.setItems(distributordata);

                    DistributorID.add(
                            Integer.toString(Math.round(Integer.parseInt(filters.get(i).getDistributorid()))));
                    DistributorID = new ArrayList<String>(new LinkedHashSet<String>(DistributorID));

                }


            } else if (SelecetedFiterText.equals("Order Date")) {
                for (int i = 0; i < filters.size(); i++) {

                    String string = filters.get(i).getOrderedDate();
                    String[] splitdate = string.split("T");
                    distributordata.add(splitdate[0]);

                    statushs.addAll(distributordata);
                    distributordata = new ArrayList<String>(new LinkedHashSet<String>(distributordata));


                    filter.setItems(distributordata);
                    DistributorID.add(
                            Integer.toString(Math.round(Integer.parseInt(filters.get(i).getDistributorid()))));
                    DistributorID = new ArrayList<String>(new LinkedHashSet<String>(DistributorID));

                }
            } else if(SelecetedFiterText.equals("Order Number")){
                for (int i = 0; i < filters.size(); i++) {

                    distributordata.add(filters.get(i).getOrderNumber().toString());
                    statushs.addAll(distributordata);
                    distributordata = new ArrayList<String>(new LinkedHashSet<String>(distributordata));
                    filter.setItems(distributordata);
                    DistributorID.add(
                            Integer.toString(Math.round(Integer.parseInt(filters.get(i).getDistributorid()))));
                    // DistributorID = new ArrayList<String>(new LinkedHashSet<String>(DistributorID));
                }
            }

        }
    }
    // for Distributor Name
    private void GetCompletedOrderDetailsForFilter() {

        final ProgressDialog loading = ProgressDialog.show(Dealer_Completed_Order_Activity.this,
                "Loading", "Please wait", false, false);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();

        APIService api = retrofit.create(APIService.class);
        Call<OrderDetails> filtersCall;

        if (SelecetedFiterText.equals("Distributor Name")) {
            filtersCall = api.GetCompletedOrderDetailsForName(DealerID, SelecetedFiterText.toString(),
                    SelectedFilter.toString(), SelectedDistributorID);
        } else if (SelecetedFiterText.equals("Order Date")) {
            filtersCall = api.GetCompletedOrderDetailsForOrderDate(DealerID, SelecetedFiterText.toString(),
                    SelectedFilter.toString());
            Log.v("Selected Date", SelectedFilter.toString());
        } else {
            filtersCall = api.GetCompletedOrderDetailsForOrderNumber(DealerID, SelecetedFiterText.toString(),
                    SelectedFilter.toString());
        }


        filtersCall.enqueue(new Callback<OrderDetails>() {
            @Override
            public void onResponse(Call<OrderDetails> call, Response<OrderDetails> response) {

                loading.dismiss();

                if (response.isSuccessful()) {
                    String Status = response.body().getResult();
                    Order_result = response.body().getData();
                    if (Order_result != null)
                        LoadListviewData();

                } else {
                    loading.dismiss();
                    // handle request errors yourself
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<OrderDetails> call, Throwable t) {
                loading.dismiss();
                t.printStackTrace();
                box.showAlertbox(getResources().getString(R.string.server_error));

            }
        });

    }

    private void LoadListviewData() {

        adapter = new Completed_Order_Adapter(Dealer_Completed_Order_Activity.this, Order_result);
        listView.setAdapter(adapter);

    }
}
