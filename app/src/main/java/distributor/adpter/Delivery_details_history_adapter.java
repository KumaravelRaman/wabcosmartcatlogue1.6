package distributor.adpter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.List;

import models.distributor.Distributor_history_Model;

/**
 * Created by system01 on 5/12/2017.
 */

public class Delivery_details_history_adapter extends RecyclerView.Adapter<Delivery_details_history_adapter.HistoryViewHolder>{

    List<Distributor_history_Model> itemList;
    Context context;

    public Delivery_details_history_adapter(Context context, List<Distributor_history_Model> itemList)
    {
        this.context=context;
        this.itemList=itemList;
    }
    @Override
    public HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.distributor_delivery_details_history_card_row, null);
        HistoryViewHolder rcv = new HistoryViewHolder(layoutView);
        return rcv;
    }
    @Override
    public void onBindViewHolder(HistoryViewHolder holder, int position)
    {
        holder.orderNumber.setText(itemList.get(position).getOrderNumber());
        holder.deliveryName.setText(itemList.get(position).getDeliveryAddress());
        holder.deliveryDate.setText(itemList.get(position).getDeliveryDate());
        holder.viewbtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                
            }
        });
    }
    @Override
    public int getItemCount()
    {
        return this.itemList.size();
    }

    public static class HistoryViewHolder extends RecyclerView.ViewHolder {


        protected TextView orderNumber;
        protected TextView deliveryName;
        protected TextView deliveryDate;
        protected Button viewbtn;

        public HistoryViewHolder(View v)
        {
            super(v);
            orderNumber =  (TextView) v.findViewById(R.id.order_number);
            deliveryName = (TextView)  v.findViewById(R.id.buyer_name);
            deliveryDate = (TextView)  v.findViewById(R.id.delivery_date);
            viewbtn = (Button) v.findViewById(R.id.btn_details_view);
        }
    }


}
