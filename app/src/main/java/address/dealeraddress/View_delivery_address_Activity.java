package address.dealeraddress;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.wabco.brainmagic.wabco.catalogue.R;


public class View_delivery_address_Activity extends Activity {


    private View heade_Layout;
    private TextView Tittle;
    private ImageView Backbtn;
    private Button AddNewBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_delivery_address);

        heade_Layout =  findViewById(R.id.header_layout);
        Tittle = (TextView) heade_Layout.findViewById(R.id.tittle);
        Backbtn = (ImageView) heade_Layout.findViewById(R.id.back);

        AddNewBtn = (Button) findViewById(R.id.new_address);

        Tittle.setText("Deleivery Address");


        Backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        AddNewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(View_delivery_address_Activity.this,Add_delivery_address_Activity.class));
            }
        });

    }

}
