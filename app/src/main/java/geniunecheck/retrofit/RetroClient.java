package geniunecheck.retrofit;

import api.APIService;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Sathriyan on 9/21/2017.
 */
public class RetroClient {
//    private static final String ROOT_URL = "http://genuinecheck.wabcoindia-am.com/";   //current URL

    private static final String ROOT_URL = "https://genuinecheck.wabcoindia-am.com/gueninecheck/";


//    private static final String ROOT_URL = "https://genuinecheck_test.wabcoindia-am.com/";
//private static final String ROOT_URL = "https://genuinechecktest.wabcoindia-am.com/gen/"; // wabco server


//    https://genuinecheck_test.wabcoindia-am.com/gen/api/MOM/getAllDistributors
    /**
     * Get Retrofit Instance
     */
    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    /**
     * Get API Service
     *
     * @return API Service
     */
    public static APIService getApiService() {
        return getRetrofitInstance().create(APIService.class);
    }
}
