package geniunecheck.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.mancj.materialsearchbar.MaterialSearchBar;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import alertbox.Alertbox;
import api.APIService;
import api.ApiUtils;
import geniunecheck.scan.Scan;
import models.genunecheck.SerialData;
import models.genunecheck.SerialList;
import network.NetworkConnection;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Serial_List_Activity extends AppCompatActivity {


    private Alertbox box = new Alertbox(Serial_List_Activity.this);
    private String mSixDigit;
    private ListView mListSerial;
    private Button mSelect;
    private String mSerialPosition;
    private boolean IsSelected;
    private MaterialSearchBar mSearchBar;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serial__list_);

        mSixDigit = getIntent().getStringExtra("mSixDigit");
        mListSerial = (ListView) findViewById(R.id.list);
        mSelect = (Button) findViewById(R.id.select);
        mSearchBar = (MaterialSearchBar) findViewById(R.id.searchBar);

        mListSerial.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                IsSelected = true;
                mSerialPosition = parent.getAdapter().getItem(position).toString();
            }
        });

        mSearchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (IsSelected) {
                    IsSelected = false;
                    Intent a = new Intent(Serial_List_Activity.this, Scan.class);
                    a.putExtra("Serial id", mSerialPosition);
                    startActivity(a);
                } else {
                    StyleableToast st = new StyleableToast(Serial_List_Activity.this, "Select one serial number", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(Serial_List_Activity.this.getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }


            }
        });

        NetworkConnection isnet = new NetworkConnection(Serial_List_Activity.this);
        if (isnet.CheckInternet()) {
            getCompleteSerialNO();
        } else {
            box.showAlertbox(getResources().getString(R.string.nointernetmsg));
        }
    }

    private static OkHttpClient okClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.MINUTES)
                .writeTimeout(15, TimeUnit.MINUTES)
                .readTimeout(15, TimeUnit.MINUTES)
                .build();
    }

    private void getCompleteSerialNO() {

        final ProgressDialog loading = ProgressDialog.show(Serial_List_Activity.this,
                "Loading", "Please wait...", false, false);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiUtils.BASE_URL)
                .client(okClient())
                .addConverterFactory(GsonConverterFactory.create()).build();

        APIService api = retrofit.create(APIService.class);
        Call<SerialList> filtersCall = api.GetautoPartsearch(mSixDigit);
        filtersCall.enqueue(new Callback<SerialList>() {
            @Override
            public void onResponse(Call<SerialList> call, Response<SerialList> response) {

                loading.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getResult().equals("success")) {
                        if (!response.body().getData().isEmpty()) {

                            ArrayList<String> seriList = new ArrayList<>();
                            List<SerialData> data = response.body().getData();
                            for (SerialData datum : data) {
                                seriList.add(datum.getPartid());
                            }
                         adapter =  new ArrayAdapter<String>(
                                    Serial_List_Activity.this,
                                    android.R.layout.simple_list_item_single_choice,
                                    seriList);
                            mListSerial.setAdapter(adapter);
                        }
                    } else box.showNegativebox("Serial Number not found!");
                } else {
                    loading.dismiss();
                    // handle request errors yourself
                    box.showNegativebox(getResources().getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<SerialList> call, Throwable t) {
                loading.dismiss();
                t.printStackTrace();
                box.showNegativebox(getResources().getString(R.string.server_error));

            }
        });

    }


}
