package geniunecheck.gcadapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;

import com.wabco.brainmagic.wabco.catalogue.R;

import java.util.List;

import api.CustomItemClickListener;

public class SerailListArrayAdapter extends ArrayAdapter<String> {

    private final List<String> list;
    private final Activity context;
    private CustomItemClickListener listener;
    static class ViewHolder {
        protected RadioButton serial;

    }

    public SerailListArrayAdapter(Activity context, List<String> list,CustomItemClickListener listener) {
        super(context, R.layout.adapter_serial_row, list);
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = null;

        if (convertView == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.adapter_serial_row, null);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.serial = (RadioButton) view.findViewById(R.id.serial);
            view.setTag(viewHolder);
            /*viewHolder.serial.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked) {
                        listener.onItemClick(buttonView, position);
                        list.get(position).setSelected(isChecked);
                    }
                    else
                        list.get(position).setSelected(false);
                }
            });*/
        } else {
            view = convertView;
        }

        ViewHolder holder = (ViewHolder) view.getTag();
        holder.serial.setText(list.get(position).toString());



        return view;
    }
}
