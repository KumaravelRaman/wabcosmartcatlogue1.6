package pekit;

import java.util.List;

public class PEkitDTO {
    private List<String> bomDescriptionList;
    private List<String> bomPartNoList;
    private List<String> bomqtylist;
    private List<String> descrip;
    private List<String> partcodeno;
    private List<String> partnoList;
	private List<String> purposeDescp;
	private List<String>  flaglist;
	private List<String> usedDescp;

    public List<String> getPriceflaglist() {
        return priceflaglist;
    }
    public void setPriceflaglist(List<String> priceflaglist) {
        this.priceflaglist = priceflaglist;
    }
    private List<String> priceflaglist;

    public void setPartnoList(List<String> partnoList) {
        this.partnoList = partnoList;
    }

    public void setPartcodenoList(List<String> partcodeno) {
        this.partcodeno = partcodeno;
    }

    public List<String> getpartnoList() {
        return this.partnoList;
    }

    public List<String> getpartcodeno() {
        return this.partcodeno;
    }

    public void setDescripList(List<String> descrip) {
        this.descrip = descrip;
    }

    public List<String> getdescrip() {
        return this.descrip;
    }

    public void setPartNoList(List<String> bomPartNoList) {
        this.bomPartNoList = bomPartNoList;
    }

    public void setPartDespcriptionList(List<String> bomDescriptionList) {
        this.bomDescriptionList = bomDescriptionList;
    }

    public void setProductservicepartNOList(List<String> bomqtylist) {
        this.bomqtylist = bomqtylist;
    }

    public List<String> getPartNoList() {
        return this.bomPartNoList;
    }

    public List<String> getdescriptionList() {
        return this.bomDescriptionList;
    }

    public List<String> getqtList() {
        return this.bomqtylist;
    }

	public void setPurposeDescpList(List<String> purposeDescp) {
		// TODO Auto-generated method stub
		this.purposeDescp  = purposeDescp;
	} 
	
	
	 public List<String> getPurposeDescpList() {
	        return this.purposeDescp;
	    }

	public void setFlagList(List<String> flaglist) {
		// TODO Auto-generated method stub
		this.flaglist = flaglist;
	}
	
	public List<String> getFlagList() {
		// TODO Auto-generated method stub
		 
		 return this.flaglist;
		
	}

	public void setusedAssList(List<String> usedDescp) {
		// TODO Auto-generated method stub
		this.usedDescp  = usedDescp;
	}
	
	
	 public List<String> getUsedAsspList() {
	        return this.usedDescp;
	    }

	
}
